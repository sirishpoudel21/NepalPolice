package com.NepalPolice.UserEntity;

import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import java.util.*;

@Entity
@Table(name = "user_view_account")
@Setter
@Getter
@NoArgsConstructor
@AllArgsConstructor
public class UserViewAccount implements UserDetails {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "id", insertable = false, updatable = false, nullable = false)
    private UUID id;

    @Column(name = "username", columnDefinition = "VARCHAR(64)", nullable = false)
    private String userName;

    @Column(name = "password", columnDefinition = "VARCHAR(255)", nullable = false)
    private String password;

    @Column(name = "email", columnDefinition = "VARCHAR(120)", nullable = false)
    private String email;

    @Column(name = "first_name", columnDefinition = "VARCHAR(64)", nullable = false)
    private String userFirstName;

    @Column(name = "last_name", columnDefinition = "VARCHAR(64)", nullable = false)
    private String userLastName;

    @Transient
    private String userMainRole = "";

    @Override
    public Collection<? extends GrantedAuthority> getAuthorities() {
        String role = userMainRole.isEmpty()?"ROLE_USER":userMainRole;
        switch (role){
            case "ROLE_PROJECT_MANAGER":
            case "ROLE_SUPER_ADMIN":
            case "ROLE_ADMIN":
                return Collections.singletonList(new SimpleGrantedAuthority(role));
            default:
                return Collections.singletonList(new SimpleGrantedAuthority("ROLE_USER"));
        }
    }

    @Override
    public String getPassword() {
        return this.password;
    }

    @Override
    public String getUsername() {
        return this.userName;
    }

    @Override
    public boolean isAccountNonExpired() {
        return true;
    }

    @Override
    public boolean isAccountNonLocked() {
        return true;
    }

    @Override
    public boolean isCredentialsNonExpired() {
        return true;
    }

    @Override
    public boolean isEnabled() {
        return true;
    }
}
