package com.NepalPolice.Entities;

import com.fasterxml.jackson.annotation.JsonIgnore;
import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.UUID;

@Entity
@Table(name = "preceding_activity")
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class PrecedingActivity {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name="id", insertable = false, updatable = false, nullable = false)
    private UUID id;


    @ManyToOne
    @JsonIgnore
    private Activity activity;

    @ManyToOne
    @JsonIgnore
    private Activity preceding_activity;

}
