package com.NepalPolice.Entities;

import com.fasterxml.jackson.annotation.JsonIgnore;
import jakarta.persistence.*;

import java.util.UUID;

@Entity
@Table(name = "Municipality_Master_Channel")
public class MunicipalityMasterEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name="id", insertable = false, updatable = false, nullable = false)
    private UUID id;

    @Column(name = "Municipality",columnDefinition = "VARCHAR(60)",nullable = false)
    private String muncipalityName;

    @ManyToOne
    @JsonIgnore
    private DistrictMChannel districtMChannel;
}
