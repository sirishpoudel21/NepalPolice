package com.NepalPolice.Entities;

import lombok.*;

//@Data
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@ToString
@Builder
public class LoginCrediential {
    private  String username;
    private String password;
}
