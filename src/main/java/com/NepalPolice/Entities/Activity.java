package com.NepalPolice.Entities;

import com.NepalPolice.ENUMS.Status;
import com.NepalPolice.ENUMS.VerifyStatus;
import com.fasterxml.jackson.annotation.JsonIgnore;
import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.UUID;

@Entity
@Table(name = "Activities")
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class Activity {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name="id", insertable = false, updatable = false, nullable = false)
    private UUID id;

    @Column(name = "activity_name", columnDefinition = "VARCHAR(255)", nullable = false)
    private String activityName;

    @Column(name = "description",columnDefinition = "VARCHAR2(500)", nullable = false)
    private String activityDescription;

    @Column(name = "file_path", columnDefinition = "VARCHAR(255)")
    private String filePath;

    @Column(name = "priority", columnDefinition = "int", nullable = false)
    private int activityPriority;

    @Column(name = "created_at", columnDefinition = "Date",nullable = false)
    private Date created_at;

    @PrePersist
    protected void onCreate() {
        created_at = new Date();
    }


    @Column(name = "province",columnDefinition = "VARCHAR(255)", nullable = false)
    private String province;

    @Column(name = "district",columnDefinition = "VARCHAR(255)", nullable = false)
    private String district;

    @Column(name = "municipality",columnDefinition = "VARCHAR(255)", nullable = false)
    private String municipality;

    @Enumerated(EnumType.STRING)
    private Status activityStatus;

    @Enumerated(EnumType.STRING)
    private VerifyStatus verifyStatus;

    @ManyToOne
    @JoinColumn(name = "task_id", referencedColumnName = "id")
    @JsonIgnore
    private Task task;

    @OneToMany(mappedBy = "activity", cascade = CascadeType.ALL, fetch = FetchType.EAGER)
    private List<PrecedingActivity> activitiesID = new ArrayList<>();

    @OneToMany(mappedBy = "preceding_activity", cascade = CascadeType.ALL,  fetch = FetchType.EAGER)
    private List<PrecedingActivity> PrecedingActivity = new ArrayList<>();
}
