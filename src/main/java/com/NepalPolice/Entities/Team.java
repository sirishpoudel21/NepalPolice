package com.NepalPolice.Entities;

import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

@Entity
@Table(name = "team")
@Setter
@Getter
@NoArgsConstructor
@AllArgsConstructor
public class Team {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name="id", insertable = false, updatable = false, nullable = false)
    private UUID id;

    @Column(name = "team_name",columnDefinition = "VARCHAR(128)", nullable = false)
    private String teamName;

    @OneToMany(mappedBy = "team",cascade = CascadeType.REMOVE, fetch = FetchType.EAGER, orphanRemoval = true)
    private List<TeamMember> teamMembers = new ArrayList<>();

    @OneToOne(mappedBy = "team",cascade = CascadeType.REMOVE, fetch = FetchType.EAGER, orphanRemoval = true)
    private Project project;
}
