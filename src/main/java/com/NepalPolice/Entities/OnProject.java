package com.NepalPolice.Entities;

import com.fasterxml.jackson.annotation.JsonIgnore;
import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.Date;
import java.util.UUID;

@Entity
@Table(name = "on_project")
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class OnProject {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name="id", insertable = false, updatable = false, nullable = false)
    private UUID id;

    @ManyToOne
    @JsonIgnore
    private Project project;

    @Column(name = "date_started",columnDefinition = "DATE",nullable = false)
    private Date dateStarted;

    @Column (name = "date_ended", columnDefinition = "DATE")
    private  Date dateEnded;

    @Column(name = "project_description",columnDefinition = "VARCHAR(255)", nullable = false)
    private String projDescription;

//    @Column(name = "decription",columnDefinition = "VARCHAR2(500)")
//    private String description;


}
