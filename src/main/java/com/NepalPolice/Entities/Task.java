package com.NepalPolice.Entities;


import com.NepalPolice.ENUMS.Status;
import com.fasterxml.jackson.annotation.JsonIgnore;
import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.UUID;

@Entity
@Table(name = "Tasks")
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class Task {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name="id", insertable = false, updatable = false, nullable = false)
    private UUID id;

    @Column(name = "task_name",columnDefinition = "VARCHAR(255)", nullable = false)
    private String taskName;

    @Column(name = "priority", columnDefinition = "NUMBER", nullable = false)
    private int taskPriority;

    @Column(name = "description", columnDefinition = "VARCHAR2(500)")
    private String taskDescription;

    @Column(name = "planned_start_date", columnDefinition = "Date")
    private Date taskPlannedStartDate;

    @Column(name = "planned_end_date", columnDefinition = "Date")
    private Date taskPlannedEndDate;

    @Column(name = "planned_budget", columnDefinition = "decimal(8,2)")
    private double taskPlannedBudget;

    @Column(name = "actual_start_date", columnDefinition = "Date")
    private Date taskActualStartDate;

    @Column(name = "actual_end_date", columnDefinition = "Date")
    private Date taskActualEndDate;

    @Column(name = "actual_budget", columnDefinition = "decimal(8,2)")
    private double taskActualBudget;

    @Column(name = "province",columnDefinition = "VARCHAR(255)", nullable = false)
    private String province;

    @Column(name = "district",columnDefinition = "VARCHAR(255)", nullable = false)
    private String district;

    @Column(name = "municipality",columnDefinition = "VARCHAR(255)", nullable = false)
    private String municipality;

    @Enumerated(EnumType.STRING)
    private Status taskStatus;

    @OneToMany(mappedBy = "task",cascade = CascadeType.REMOVE, fetch = FetchType.EAGER,orphanRemoval = true)
    private List<Activity> activities = new ArrayList<>();

    @ManyToOne
    @JoinColumn(name = "project_id",referencedColumnName = "id")
    @JsonIgnore
    private Project project;

    @OneToMany(mappedBy = "task", cascade = CascadeType.REMOVE, fetch = FetchType.EAGER, orphanRemoval = true)
    private List<Assigned> Assigned = new ArrayList<>();

    @OneToMany(mappedBy = "task", cascade = CascadeType.ALL , fetch = FetchType.EAGER)
    private List<PrecedingTask> precedingTasks =  new ArrayList<>();

    @OneToMany(mappedBy = "preceding_task", cascade = CascadeType.ALL,  fetch = FetchType.EAGER)
    private List<PrecedingTask> PrecedingTaskId = new ArrayList<>();
}
