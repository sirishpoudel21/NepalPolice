package com.NepalPolice.Entities;

import jakarta.persistence.*;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.UUID;

@Entity
@Table(name = "LOCATION_MASTER_CHANNEL")
public class LocationMasterChannel {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name="id", insertable = false, updatable = false, nullable = false)
    private UUID id;

    @Column(name = "provience",columnDefinition = "VARCHAR(18)",nullable = false)
    private String provienceName;

    @OneToMany(mappedBy = "locationMasterChannel",cascade = CascadeType.REMOVE, fetch = FetchType.EAGER, orphanRemoval = true)
    private List<DistrictMChannel> districtMChannels = new ArrayList<>();
}
