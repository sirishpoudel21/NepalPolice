package com.NepalPolice.Entities;

import com.NepalPolice.ENUMS.Status;
import com.fasterxml.jackson.annotation.JsonIgnore;
import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.*;

@Entity
@Table(name = "projects")
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class Project {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name="id", insertable = false, updatable = false, nullable = false)
    private UUID id;

    @Column(name = "project_name",columnDefinition = "VARCHAR(128)", nullable = false)
    private String projName;

    @Column(name = "planned_start_date", columnDefinition = "Date")
    private Date plannedStartDate;

    @Column(name = "planned_end_date", columnDefinition = "Date")
    private Date plannedEndDate;


    @Column(name = "actual_start_date", columnDefinition = "Date")
    private Date startDate;

    @Column(name = "actual_end_date", columnDefinition = "Date")
    private Date endDate;

    @Column(name = "project_description",columnDefinition = "VARCHAR(255)", nullable = false)
    private String projDescription;

    @Column(name = "province",columnDefinition = "VARCHAR(255)", nullable = false)
    private String province;

    @Column(name = "district",columnDefinition = "VARCHAR(255)", nullable = false)
    private String district;

    @Column(name = "municipality",columnDefinition = "VARCHAR(255)", nullable = false)
    private String municipality;

    @Enumerated(EnumType.STRING)
    private Status projectStatus;

    @OneToOne
    @JoinColumn(name = "team_id", referencedColumnName = "id")
    @JsonIgnore
    private Team team;

    @OneToMany(mappedBy = "project",cascade = CascadeType.REMOVE, fetch = FetchType.EAGER,orphanRemoval = true)
    private List<Task> task = new ArrayList<>();

    @OneToMany(mappedBy = "project",cascade = CascadeType.ALL, fetch = FetchType.EAGER)
    private List<ProjectManager> projectManagers = new ArrayList<>();

    @OneToMany(mappedBy = "project",cascade = CascadeType.ALL, fetch = FetchType.EAGER)
    private List<OnProject> projectsDescription = new ArrayList<>();
}
