package com.NepalPolice.Entities;

import com.NepalPolice.ENUMS.UserStatus;
import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.UUID;

@Entity
@Table(name = "employees")
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class Employee {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name="id", insertable = false, updatable = false, nullable = false)
    private UUID id;

    @Column(name = "employee_code", columnDefinition = "VARCHAR(128)",nullable = false)
    private String employee_code;

    @Column(name = "password", columnDefinition = "VARCHAR(128)",nullable = false)
    private String password;

    @Enumerated(EnumType.STRING)
    private UserStatus userStatus;

    @Column(name = "employee_name", columnDefinition = "VARCHAR(128)", nullable = false)
    private String employee_name;

//    @OneToMany(mappedBy = "employee",cascade = CascadeType.ALL)
//    private List<TeamMember> teamMembers = new ArrayList<>();
}
