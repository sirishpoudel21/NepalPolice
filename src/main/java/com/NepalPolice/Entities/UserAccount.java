package com.NepalPolice.Entities;


import com.NepalPolice.ENUMS.UserStatus;
import com.fasterxml.jackson.annotation.JsonIgnore;
import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import java.util.*;

@Entity
@Table(name = "user_account")
@Setter
@Getter
@NoArgsConstructor
@AllArgsConstructor
public class UserAccount {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "id", insertable = false, updatable = false, nullable = false)
    private UUID id;

    @Column(name = "user_main_role", columnDefinition = "VARCHAR(64)", nullable = false)
    private String userMainRole;

    @Column(name = "user_view_account_id")
    @JsonIgnore
    private UUID userViewAccountId;

    @Enumerated(EnumType.STRING)
    private UserStatus status;

    @OneToMany(mappedBy = "user_account", cascade = CascadeType.ALL)
    private List<ProjectManager> projectManagers = new ArrayList<>();

}
