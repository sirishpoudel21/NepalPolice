package com.NepalPolice.Entities;

import com.fasterxml.jackson.annotation.JsonIgnore;
import jakarta.persistence.*;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

@Entity
@Table(name = "District_Master")
public class DistrictMChannel {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name="id", insertable = false, updatable = false, nullable = false)
    private UUID id;

    @Column(name = "District",columnDefinition = "VARCHAR(30)",nullable = false)
    private String districtName;

    @ManyToOne
    @JsonIgnore
    private LocationMasterChannel locationMasterChannel;

    @OneToMany(mappedBy = "districtMChannel",cascade = CascadeType.REMOVE, fetch = FetchType.EAGER, orphanRemoval = true)
    private List<MunicipalityMasterEntity> municipalityMasterChannel = new ArrayList<>();

}
