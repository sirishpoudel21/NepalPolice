package com.NepalPolice.Entities;

import com.fasterxml.jackson.annotation.JsonIgnore;
import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

@Entity
@Table(name = "assignedTeamMember")
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class AssignedTeamMember {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name="id", insertable = false, updatable = false, nullable = false)
    private UUID id;

    @ManyToOne
    @JsonIgnore
    private TeamMember teamMember;
    @ManyToOne
    @JsonIgnore
    private Assigned assigned;

}
