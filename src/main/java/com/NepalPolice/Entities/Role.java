package com.NepalPolice.Entities;


import com.NepalPolice.ENUMS.AccessModifier;
import com.NepalPolice.ENUMS.RoleCode;
import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.experimental.Accessors;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

@Entity
@Table(name = "role_Master_Channel")
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Accessors(chain = true)
public class Role {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name="id", insertable = false, updatable = false, nullable = false)
    private UUID id;


    @Column(name = "role_name",columnDefinition = "VARCHAR(128)",nullable = false)
    private String role_name;

    @Enumerated(EnumType.STRING)
    private RoleCode roleCode;

    @Enumerated(EnumType.STRING)
    private AccessModifier accessModifier;

    @OneToMany(mappedBy = "role",cascade = CascadeType.REMOVE, fetch = FetchType.EAGER)
    private List<TeamMember> teamMembers = new ArrayList<>();

}
