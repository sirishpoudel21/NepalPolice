package com.NepalPolice.Entities;

import com.fasterxml.jackson.annotation.JsonIgnore;
import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

@Entity
@Table(name = "Assigned")
@Setter
@Getter
@NoArgsConstructor
@AllArgsConstructor
public class Assigned {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name="id", insertable = false, updatable = false, nullable = false)
    private UUID id;

    @ManyToOne
    @JsonIgnore
    private Task task;

    @OneToMany(mappedBy = "assigned", cascade = CascadeType.REMOVE,fetch = FetchType.EAGER)
    private List<AssignedTeamMember> assignedTeamMembers = new ArrayList<>();

}
