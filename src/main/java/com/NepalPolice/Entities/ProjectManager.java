package com.NepalPolice.Entities;

import com.NepalPolice.UserEntity.UserViewAccount;
import com.fasterxml.jackson.annotation.JsonIgnore;
import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.UUID;

@Entity
@Table(name = "project_manager")
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class ProjectManager {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name="id", insertable = false, updatable = false, nullable = false)
    private UUID id;

    @ManyToOne
    @JoinColumn(name = "project_id", referencedColumnName = "id")
    @JsonIgnore
    private Project project;

    @ManyToOne
    @JoinColumn(name = "user_account_id", referencedColumnName = "id")
    @JsonIgnore
    private UserAccount user_account;
}
