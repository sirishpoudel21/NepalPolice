package com.NepalPolice.UserRepositery;


import com.NepalPolice.UserEntity.UserViewAccount;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

@Repository
public interface UserViewRepo extends JpaRepository<UserViewAccount, UUID> {
    @Query("select u from UserViewAccount u where u.userName = :username")
    UserViewAccount findByUserName(String username);

    @Query("SELECT u FROM UserViewAccount u WHERE u.userName LIKE CONCAT('%', :username, '%')")
    List<UserViewAccount> findAllByUserName(String username);

//    @Query("SELECT u FROM UserViewAccount u WHERE LOWER(u.userName) LIKE LOWER(CONCAT('%', :partialUsername, '%'))")
//    List<UserViewAccount> findByUsernameContainingIgnoreCase(String partialUsername);
}