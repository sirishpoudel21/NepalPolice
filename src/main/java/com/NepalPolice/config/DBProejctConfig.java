package com.NepalPolice.config;


import jakarta.persistence.EntityManagerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;
import org.springframework.core.env.Environment;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.jdbc.datasource.DriverManagerDataSource;
import org.springframework.orm.jpa.JpaTransactionManager;
import org.springframework.orm.jpa.JpaVendorAdapter;
import org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean;
import org.springframework.orm.jpa.vendor.HibernateJpaVendorAdapter;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.annotation.EnableTransactionManagement;

import javax.sql.DataSource;
import java.util.HashMap;

@Configuration
@EnableTransactionManagement
@EnableJpaRepositories(
        entityManagerFactoryRef = "projectEntityManagementFactory",
        basePackages = {"com.NepalPolice.Repositery"},
        transactionManagerRef = "projectTransactionManager"
)
public class DBProejctConfig {
    @Autowired
    Environment env;

    @Primary
    @Bean(name = "ProjectDataSource")
    public DataSource dataSource(){
        DriverManagerDataSource ds = new DriverManagerDataSource();
        ds.setUrl(env.getProperty("spring.projectManagement.datasource.url"));
        ds.setUsername(env.getProperty("spring.projectManagement.datasource.username"));
        ds.setPassword(env.getProperty("spring.projectManagement.datasource.password"));
        ds.setDriverClassName(env.getProperty("spring.projectManagement.datasource.driver-class-name"));
        return ds;
    }

    @Primary
    @Bean(name = "projectEntityManagementFactory")
    public LocalContainerEntityManagerFactoryBean entityManager(){
        LocalContainerEntityManagerFactoryBean bean = new LocalContainerEntityManagerFactoryBean();
        bean.setDataSource(dataSource());
        JpaVendorAdapter adapter = new HibernateJpaVendorAdapter();
        bean.setJpaVendorAdapter(adapter);
        HashMap<String, Object> properties = new HashMap<String,Object>();
        properties.put("hibernate.hbm2ddl.auto","update");
        properties.put("jpa.show-sql","true");
        properties.put("hibernate.dialect","org.hibernate.dialect.OracleDialect");
        bean.setJpaPropertyMap(properties);
        bean.setPackagesToScan("com.NepalPolice.Entities");
        return bean;
    }

    @Primary
    @Bean(name = "projectTransactionManager")
    public PlatformTransactionManager transactionManager(@Qualifier("projectEntityManagementFactory")EntityManagerFactory entityManagerFactory){
        return new JpaTransactionManager(entityManagerFactory);
    }
}
