package com.NepalPolice.Payloads;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class UserViewDto {
    private String userName;
    private String password;
    private String email;
    private String userFirstName;
    private String userLastName;
}
