package com.NepalPolice.Payloads;

import com.NepalPolice.ENUMS.AccessModifier;
import com.NepalPolice.ENUMS.RoleCode;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Setter
@Getter
@NoArgsConstructor
@AllArgsConstructor
public class RoleDto {
    private String role_name;
    private RoleCode roleCode;
    private AccessModifier accessModifier;
}
