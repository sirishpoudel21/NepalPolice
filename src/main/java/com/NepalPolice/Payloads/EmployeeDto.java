package com.NepalPolice.Payloads;

import com.NepalPolice.ENUMS.UserStatus;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class EmployeeDto {
    private String employee_code;
    private String password;
    private UserStatus userStatus;
    private String employee_name;
}
