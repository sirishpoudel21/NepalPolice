package com.NepalPolice.Payloads;

import jakarta.persistence.Column;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.Date;
import java.util.UUID;

@Setter
@Getter
@NoArgsConstructor
@AllArgsConstructor
public class TaskDTO {
    private String projectID;
    private String taskName;
    private int taskPriority;
    private String taskDescription;
    private String province;
    private String district;
    private String municipality;
    private Date taskPlannedStartDate;
    private Date taskPlannedEndDate;
    private double taskPlannedBudget;
    private Date taskActualStartDate;
    private Date taskActualEndDate;
    private double taskActualBudget;
}
