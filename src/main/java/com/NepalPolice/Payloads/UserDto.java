package com.NepalPolice.Payloads;

import com.NepalPolice.Entities.Role;
import com.NepalPolice.Entities.UserAccount;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.Set;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class UserDto {
    private String userViewAccountId;
    private String userMainRole;
}
