package com.NepalPolice.Payloads;

import com.NepalPolice.ENUMS.Status;
import com.NepalPolice.ENUMS.VerifyStatus;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.springframework.web.multipart.MultipartFile;

import java.util.Date;
import java.util.List;


@Setter
@Getter
@NoArgsConstructor
@AllArgsConstructor
public class ActivitiesDTO {
    private String activityName;
    private String taskId;
    private String activityDescription;
    private String province;
    private String district;
    private String municipality;
    private int activityPriority;
    private Status activityStatus;
//    private MultipartFile files;
    private VerifyStatus verifyStatus;
}
