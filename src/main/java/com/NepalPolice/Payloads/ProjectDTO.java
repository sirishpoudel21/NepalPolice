package com.NepalPolice.Payloads;

import jakarta.persistence.Column;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.Date;
import java.util.UUID;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class ProjectDTO {
    private UUID teamId;
    private String projName;
    private Date plannedStartDate;
    private Date plannedEndDate;
    private Date startDate;
    private Date endDate;
    private String projDescription;
    private String province;
    private String district;
    private String municipality;
}
