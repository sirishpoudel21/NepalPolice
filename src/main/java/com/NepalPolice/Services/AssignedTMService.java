package com.NepalPolice.Services;

import com.NepalPolice.Entities.AssignedTeamMember;
import com.NepalPolice.Entities.Project;
import com.NepalPolice.Payloads.AssignedTeamMemberDto;

import java.util.List;
import java.util.UUID;

public interface AssignedTMService {
    AssignedTeamMember createAssignedTeamMember(AssignedTeamMemberDto assignedTeamMemberDto);

    List<Project> getAssignedTeamMember(UUID userId, String roles);
}
