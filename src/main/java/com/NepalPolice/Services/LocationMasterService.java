package com.NepalPolice.Services;

import com.NepalPolice.Entities.Project;
import com.NepalPolice.Payloads.ProjectDTO;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

public interface LocationMasterService {
    // Create LocationProvience
    LocationMasterService createLocationProvience(LocationMasterService locationMasterService);

    // update a particular LocationProvience by its id
    void updateLocationMasterService(LocationMasterService locationMaster, UUID locationId);

    // get a single LocationMasterService
    Optional<Project> getLocationMasterServiceById(UUID locationId);

    // Get all  LocationMasterService
    List<LocationMasterService> getAllLocationMasterService();

    // delete LocationMasterService by its I'd
    void deleteLocationMasterService(UUID locationId);
}
