package com.NepalPolice.Services;


import com.NepalPolice.Entities.Activity;
import com.NepalPolice.Payloads.ActivitiesDTO;
import org.springframework.web.multipart.MultipartFile;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

public interface ActivitiesService {
    // Create Activities
    Activity createActivities(ActivitiesDTO activitiesDto,MultipartFile files);

    // update a particular Activities by its id
    void updateActivities(ActivitiesDTO activitiesDTO, UUID taskID);

    // get a single Activities by its id
    Optional<Activity> getActivitiesById(UUID ActivitiesID);

    // Get all  Activities
    List<Activity> getAllActivities();

    List<Activity> getAllActivitiesByTaskId(UUID taskId);

    List<Activity> getActivitiesForPm(UUID userId);

    // Get Activities Belong to Task
    List<ActivitiesDTO> activitiesBelongToTask(UUID activitiesId);

    // delete Activities by its I'd
    void deleteActivities(UUID activitiesId);
}
