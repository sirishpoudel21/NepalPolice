package com.NepalPolice.Services;

import com.NepalPolice.Entities.Project;
import com.NepalPolice.Entities.ProjectManager;

import java.util.List;
import java.util.UUID;

public interface ProjectManagerService {
    ProjectManager insertProjectManager(String userId,UUID projectId);
    List<Project> getProjectsForUser(UUID userId);
}
