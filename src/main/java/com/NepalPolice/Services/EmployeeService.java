package com.NepalPolice.Services;

import com.NepalPolice.Entities.Employee;
import com.NepalPolice.Payloads.ActivitiesDTO;
import com.NepalPolice.Payloads.EmployeeDto;

import java.util.List;
import java.util.UUID;

public interface EmployeeService {

    // create employee
    Employee createEmployee(EmployeeDto employeeDto);

    // update employee
    Employee updateEmployee(EmployeeDto employeeDto, UUID employeeID);

    // Get all  Employee
    List<Employee> getAllEmployee();

    // Get Employee by id
    Employee getEmployeeById(UUID employeeID);

    // delete Employee by its I'd
    void deleteEmployee(UUID employeeId);
}
