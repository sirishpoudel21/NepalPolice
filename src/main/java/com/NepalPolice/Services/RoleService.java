package com.NepalPolice.Services;

import com.NepalPolice.ENUMS.RoleCode;
import com.NepalPolice.Entities.Role;
import com.NepalPolice.Payloads.RoleDto;
import com.NepalPolice.Repositery.RoleRepo;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;

public interface  RoleService {
    Role createRole(RoleDto roleDto, String roleCode);
    List<Role> findAllRole();
    Role findById(String role);
    Role updateRole(String roleId, RoleDto roleDto, String roles);

    void deleteRoleById(String roleId);
    Role findByRoleName(String name);
}
