package com.NepalPolice.Services;


import com.NepalPolice.Entities.TeamMember;
import com.NepalPolice.Payloads.TeamMemberDto;

import java.util.List;
import java.util.UUID;

public interface TeamMemberService {
    // create TeamMember
    TeamMember createTeamMember(TeamMemberDto teamMemberDto);

    // update TeamMember
    TeamMember updateTeamMember(TeamMemberDto teamMemberDto, UUID teamMemberID);

    // Get all  TeamMember
    List<TeamMember> getAllTeamMember();

    // Get TeamMember by id
    TeamMember getTeamMemberById(UUID teamMemberID);

    // delete TeamMember by its I'd
    void deleteTeamMember(UUID teamMemberId);
}
