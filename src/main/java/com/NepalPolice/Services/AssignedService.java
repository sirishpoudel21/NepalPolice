package com.NepalPolice.Services;

import com.NepalPolice.Entities.Assigned;

import java.util.List;
import java.util.UUID;

public interface AssignedService {

    // create Activity Assigned
    Assigned createAssigned(Assigned assigned);

    // update Activity Assigned
    Assigned updateAssignment(Assigned assigned);

    // get Activity assigned by activityId
    Assigned getActivityAndAssignedToByActivityID(UUID activityID);

    // Get Activities Belong to Task
    List<Assigned> activitiesBelongToTask(UUID activityId);

    // delete Activities by its I'd
    void deleteAssigned(UUID assignedId);
}
