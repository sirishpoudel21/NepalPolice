package com.NepalPolice.Services;

import com.NepalPolice.Entities.Activity;
import com.NepalPolice.Entities.PrecedingActivity;
import com.NepalPolice.Payloads.ActivitiesDTO;

import java.util.List;
import java.util.UUID;

public interface PrecedingActivityService {
    // Create PrecedingActivity
    PrecedingActivity createPrecedingActivity(PrecedingActivity precedingActivity);

    // update a particular PrecedingActivity by its id
    PrecedingActivity updatePrecedingActivity(PrecedingActivity precedingActivity, UUID precedingActivityID);

    // get a single PrecedingActivity by activityId
    PrecedingActivity getPrecedingActivityById(UUID activityID);

    // Get All PrecedingActivity Belong to Activity
    List<PrecedingActivity> allPrecedingActivityByActivityId(UUID activityId);

    // delete Activities by its I'd
    void deletePrecedingActivity(UUID precedingActivityId);
}
