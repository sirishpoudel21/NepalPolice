package com.NepalPolice.Services;

import com.NepalPolice.Entities.OnProject;
import com.NepalPolice.Entities.Project;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.UUID;

@Service
public interface OnProjectService {
    // create OnProject
    OnProject createOnProject(Project project);

    // update OnProject
    OnProject updateOnProject(OnProject onProject, UUID onProjectID);

    // Get all  OnProject
    List<OnProject> getAllOnProject();

    // Get OnProject by id
    OnProject getOnProjectById(UUID onProjectID);

    // delete OnProject by its I'd
    void deleteOnProject(UUID onProjectId);
}
