package com.NepalPolice.Services.Implementation;

import com.NepalPolice.Entities.PrecedingActivity;
import com.NepalPolice.Repositery.PrecedingActivityRepo;
import com.NepalPolice.Services.PrecedingActivityService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.UUID;

@Service
public class PrecedingActivityServiceImpl implements PrecedingActivityService {
    @Autowired
    private PrecedingActivityRepo precedingActivityRepo;

    @Override
    public PrecedingActivity createPrecedingActivity(PrecedingActivity precedingActivity) {
        return null;
    }

    @Override
    public PrecedingActivity updatePrecedingActivity(PrecedingActivity precedingActivity, UUID precedingActivityID) {
        return null;
    }

    @Override
    public PrecedingActivity getPrecedingActivityById(UUID activityID) {
        return null;
    }

    @Override
    public List<PrecedingActivity> allPrecedingActivityByActivityId(UUID activityId) {
        return null;
    }

    @Override
    public void deletePrecedingActivity(UUID precedingActivityId) {

    }
}
