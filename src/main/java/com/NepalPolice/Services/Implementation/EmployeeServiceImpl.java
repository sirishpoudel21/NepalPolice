package com.NepalPolice.Services.Implementation;

import com.NepalPolice.ENUMS.UserStatus;
import com.NepalPolice.Entities.Employee;
import com.NepalPolice.Payloads.EmployeeDto;
import com.NepalPolice.Payloads.UserViewDto;
import com.NepalPolice.Repositery.EmployeeRepo;
import com.NepalPolice.Services.EmployeeService;
import com.NepalPolice.UserEntity.UserViewAccount;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.UUID;

@Service
public class EmployeeServiceImpl implements EmployeeService {
    @Autowired
    private EmployeeRepo employeeRepo;

    @Autowired
    private ModelMapper modelMapper;

    @Override
    public Employee createEmployee(EmployeeDto employeeDto) {
        Employee employee = dtoEmployeeToEmployee(employeeDto);
        employee.setUserStatus(UserStatus.ACTIVE);
        return employeeRepo.save(employee);
    }

    @Override
    public Employee updateEmployee(EmployeeDto employeeDto, UUID employeeID) {
        Employee employee = employeeRepo.findById(employeeID)
                .orElseThrow(() -> new IllegalArgumentException("Employee not found"));

        employee.setEmployee_code(employeeDto.getEmployee_code());
        employee.setEmployee_name(employeeDto.getEmployee_name());
        employee.setPassword(employeeDto.getPassword());
        employee.setUserStatus(employeeDto.getUserStatus());

        return employeeRepo.save(employee);
    }

    @Override
    public List<Employee> getAllEmployee() {
        return employeeRepo.findAll();
    }

    @Override
    public Employee getEmployeeById(UUID employeeID) {
        return employeeRepo.findById(employeeID)
                .orElseThrow(() -> new IllegalArgumentException("Employee not found"));
    }

    @Override
    public void deleteEmployee(UUID employeeId) {
        if (employeeRepo.existsById(employeeId)) {
            employeeRepo.deleteById(employeeId);
        } else {
            throw new IllegalArgumentException("Employee not found");
        }
    }

    public Employee dtoEmployeeToEmployee(EmployeeDto employeeDto) {
        return this.modelMapper.map(employeeDto, Employee.class);
    }
}
