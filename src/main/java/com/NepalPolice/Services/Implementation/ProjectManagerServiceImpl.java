package com.NepalPolice.Services.Implementation;

import com.NepalPolice.Entities.Project;
import com.NepalPolice.Entities.ProjectManager;
import com.NepalPolice.Entities.Team;
import com.NepalPolice.Entities.UserAccount;
import com.NepalPolice.Payloads.ProjectManagerDto;
import com.NepalPolice.Payloads.UserDto;
import com.NepalPolice.Repositery.ProjectManagerRepo;
import com.NepalPolice.Repositery.ProjectRepo;
import com.NepalPolice.Repositery.TeamRepo;
import com.NepalPolice.Repositery.UserRepo;
import com.NepalPolice.Services.ProjectManagerService;
import com.NepalPolice.UserEntity.UserViewAccount;
import com.NepalPolice.UserRepositery.UserViewRepo;
import org.modelmapper.ModelMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Collections;
import java.util.List;
import java.util.Optional;
import java.util.UUID;
import java.util.stream.Collectors;

@Service
public class ProjectManagerServiceImpl implements ProjectManagerService {
    @Autowired
    private ProjectManagerRepo projectManagerRepo;

    @Autowired
    private UserRepo userRepo;

    @Autowired
    private TeamRepo teamRepo;

    @Autowired
    private UserViewRepo userViewRepo;

    @Autowired
    private ProjectRepo projectRepo;

    @Autowired
    private ModelMapper modelMapper;

    @Override
    public ProjectManager insertProjectManager(String userId,UUID projectId) {
        UUID userIdUUID = UUID.fromString(userId);
        UserAccount userAccount = userRepo.findByUserViewAccountId(userIdUUID);
        if(userAccount==null){
            throw new IllegalArgumentException("User account not found");
        }
        Project project = projectRepo.findById(projectId)
                .orElseThrow(() -> new IllegalArgumentException("Project not found"));
        ProjectManager projectManager = new ProjectManager();
        projectManager.setUser_account(userAccount);
        projectManager.setProject(project);
        return projectManagerRepo.save(projectManager);
    }

    @Override
    public List<Project> getProjectsForUser(UUID userId) {
        UserAccount userAccount = userRepo.findByUserViewAccountId(userId);
        List<Project> projects = projectManagerRepo.findByUserAccount_Id(userAccount.getId());
        Logger logger = LoggerFactory.getLogger(getClass());

        if (!projects.isEmpty()) {
            return projects;
        } else {
            logger.warn("No projects found for user with ID: {}", userId);
            return Collections.emptyList();
        }
    }

    public UserAccount dtoProjectManagerToProjectManager(ProjectManagerDto projectManagerDto){
        return this.modelMapper.map(projectManagerDto ,UserAccount.class);
    }
}
