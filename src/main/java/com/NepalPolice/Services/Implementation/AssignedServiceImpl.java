package com.NepalPolice.Services.Implementation;

import com.NepalPolice.Entities.Assigned;
import com.NepalPolice.Entities.UserAccount;
import com.NepalPolice.Payloads.UserDto;
import com.NepalPolice.Repositery.AssignedRepo;
import com.NepalPolice.Services.AssignedService;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.UUID;

@Service
public class AssignedServiceImpl implements AssignedService {
    @Autowired
    private AssignedRepo assignedRepo;

    @Autowired
    private ModelMapper modelMapper;

    @Override
    public Assigned createAssigned(Assigned assigned) {
        return null;
    }

    @Override
    public Assigned updateAssignment(Assigned assigned) {
        return null;
    }

    @Override
    public Assigned getActivityAndAssignedToByActivityID(UUID activityID) {
        return null;
    }

    @Override
    public List<Assigned> activitiesBelongToTask(UUID activityId) {
        return null;
    }

    @Override
    public void deleteAssigned(UUID assignedId) {

    }

    public UserAccount dtoAToUser(UserDto userDTO){
        return this.modelMapper.map(userDTO ,UserAccount.class);
    }
}
