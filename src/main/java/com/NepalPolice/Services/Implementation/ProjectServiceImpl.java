package com.NepalPolice.Services.Implementation;

import com.NepalPolice.Entities.Project;
import com.NepalPolice.Entities.Team;
import com.NepalPolice.Payloads.ProjectDTO;
import com.NepalPolice.Repositery.OnProjectRepo;
import com.NepalPolice.Repositery.ProjectManagerRepo;
import com.NepalPolice.Repositery.ProjectRepo;
import com.NepalPolice.Repositery.TeamRepo;
import com.NepalPolice.Services.OnProjectService;
import com.NepalPolice.Services.ProjectService;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Service;

import java.util.*;
import java.util.stream.Collectors;

@Service
public class ProjectServiceImpl implements ProjectService {

    @Autowired
    private ProjectRepo projectRepo;

    @Autowired
    private TeamRepo teamRepo;

    @Autowired
    private ProjectManagerRepo projectManagerRepo;

    @Autowired
    private ModelMapper modelMapper;

    @Override
    public Project createProject(Project project) {
        UUID randomId = UUID.randomUUID();
        project.setId(randomId);
        return projectRepo.save(project);
    }

    @Override
    public void updateProject(ProjectDTO projectDto, UUID projectId) {
        Optional<Project> optionalProject = projectRepo.findById(projectId);
        optionalProject.ifPresent(project -> {
            project.setProjName(projectDto.getProjName());
            project.setPlannedStartDate(projectDto.getPlannedStartDate());
            project.setPlannedEndDate(projectDto.getPlannedEndDate());
            project.setStartDate(projectDto.getStartDate());
            project.setEndDate(projectDto.getEndDate());
            project.setProjDescription(projectDto.getProjDescription());
            projectRepo.save(project);
        });
    }

    @Override
    public Optional<Project> getProjectById(UUID projectId) {
        Optional<Project> getProjectById = projectRepo.findById(projectId);
        return getProjectById;
    }

    @Override
    public List<Project> getAllProjects() {
        List<Project> allProject  = this.projectRepo.findAll();
        return allProject;
    }

    @Override
    public List<Project> getProjectByTeamId(UUID teamId) {
        return projectRepo.findProjectByTeamId(teamId);
    }

    @Override
    public List<ProjectDTO> projectsBelongToManager(UUID managerID) {
        return null;
    }

    @Override
    public void deleteProject(UUID projectId) {
        projectRepo.deleteById(projectId);
    }

    // convert projectDto to project
    public Project dtoProjectToProject(ProjectDTO projectDTO){
        return this.modelMapper.map(projectDTO ,Project.class);
    }

    // Convert project To projectDTO
    public ProjectDTO projectToProjectDTO(Project project){
        return this.modelMapper.map(project, ProjectDTO.class);
    }
}
