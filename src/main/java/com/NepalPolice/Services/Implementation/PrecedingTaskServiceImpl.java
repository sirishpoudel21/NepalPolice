package com.NepalPolice.Services.Implementation;

import com.NepalPolice.Entities.PrecedingTask;
import com.NepalPolice.Repositery.PrecedingTaskRepo;
import com.NepalPolice.Services.PrecedingTaskService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.UUID;

@Service
public class PrecedingTaskServiceImpl implements PrecedingTaskService {

    @Autowired
    private PrecedingTaskRepo precedingTaskRepo;

    @Override
    public PrecedingTask createPrecedingTask(PrecedingTask precedingTask) {
        return null;
    }

    @Override
    public PrecedingTask updatePrecedingTask(PrecedingTask precedingTask, UUID precedingTaskID) {
        return null;
    }

    @Override
    public PrecedingTask getPrecedingTaskById(UUID TaskID) {
        return null;
    }

    @Override
    public List<PrecedingTask> allPrecedingTaskBYTaskID(UUID taskId) {
        return null;
    }

    @Override
    public void deletePrecedingTask(UUID precedingTaskId) {

    }
}
