package com.NepalPolice.Services.Implementation;

import com.NepalPolice.Entities.DistrictMChannel;
import com.NepalPolice.Services.DistrictMService;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

public class DistrictMServiceImpl implements DistrictMService {
    @Override
    public DistrictMChannel createDistrictMChannel(DistrictMChannel districtMChannel) {
        return null;
    }

    @Override
    public void updateDistrictMChannelService(DistrictMChannel districtMChannel, UUID districtId) {

    }

    @Override
    public Optional<DistrictMChannel> getDistrictMChannelById(UUID districtId) {
        return Optional.empty();
    }

    @Override
    public List<DistrictMChannel> getAllDistrictMChannel() {
        return null;
    }

    @Override
    public void deleteDistrictMChannel(UUID districtId) {

    }
}
