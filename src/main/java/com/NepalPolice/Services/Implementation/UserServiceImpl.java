package com.NepalPolice.Services.Implementation;

import com.NepalPolice.Entities.Employee;
import com.NepalPolice.Entities.UserAccount;
import com.NepalPolice.Payloads.UserDto;
import com.NepalPolice.Payloads.UserViewDto;
import com.NepalPolice.Repositery.EmployeeRepo;
import com.NepalPolice.Repositery.UserRepo;
import com.NepalPolice.Services.UserService;
import com.NepalPolice.UserEntity.UserViewAccount;
import com.NepalPolice.UserRepositery.UserViewRepo;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;
import java.util.UUID;
import java.util.logging.Logger;
import java.util.stream.Collectors;

@Service
public class UserServiceImpl implements UserDetailsService, UserService {

    @Autowired
    private UserRepo userRepo;

    @Autowired
    private UserViewRepo userViewRepo;

    @Autowired
    private EmployeeRepo employeeRepo;

    @Autowired
    private ModelMapper modelMapper;

    @Autowired
    private PasswordEncoder bcryptEncoder;

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        UserViewAccount userViewAccount = userViewRepo.findByUserName(username);
        if (userViewAccount == null) {
            throw new UsernameNotFoundException("User not found with username: " + username);
        }
        UserAccount userAccount = userRepo.findByUserViewAccountId(userViewAccount.getId());
        Employee employee = employeeRepo.findByEmployee_name(username);
        userViewAccount.setUserMainRole((userAccount != null) ? userAccount.getUserMainRole() : "ROLE_USER");
        return userViewAccount;
    }

    @Override
    public UserViewAccount findOne(String userID) {
//        Optional<UserViewAccount> userViewAccountOptional = userViewRepo.findById(UUID.fromString(userID));
//        if (userViewAccountOptional.isPresent()) {
//            UserViewAccount userViewAccount = userViewAccountOptional.get();
//            UserAccount userAccount = userRepo.findByUserViewAccountId(userViewAccount.getId());
//            if (userAccount != null) {
//                return userAccount;
//            } else {
//                throw new UsernameNotFoundException("No such user found with ROLE_USER role: " + userID);
//            }
//        } else {
//            throw new UsernameNotFoundException("No such user found: " + userID);
//        }
        return null;
    }

    @Override
    public List<UserViewAccount> findByUserName() {
        List<UserViewAccount> userViewAccounts = userViewRepo.findAll();
        if (userViewAccounts.isEmpty()) {
            throw new UsernameNotFoundException("No user found");
        }
        List<UserAccount> userAccounts = userRepo.findAll();

        // Extract IDs from userAccounts
        List<UUID> userIds = userAccounts.stream()
                .map(UserAccount::getUserViewAccountId)
                .collect(Collectors.toList());

        // Filter userViewAccounts based on IDs not present in userAccounts
        List<UserViewAccount> filteredUserViewAccounts = userViewAccounts.stream()
                .filter(uva -> !userIds.contains(uva.getId()))
                .collect(Collectors.toList());

        return filteredUserViewAccounts;
    }

    @Override
    public UserViewAccount findOneById(String userId) {
        Optional<UserViewAccount> userViewAccountOptional = userViewRepo.findById(UUID.fromString(userId));
        if (userViewAccountOptional.isPresent()) { // Check if the Optional contains a value
            UserViewAccount userViewAccount = userViewAccountOptional.get(); // Extract the value from the Optional
            UserAccount userAccount = userRepo.findByUserViewAccountId(userViewAccount.getId());
            if (userAccount != null && userAccount.getUserMainRole().equals("ROLE_USER")) {
                return userViewAccount;
            } else {
                throw new UsernameNotFoundException("No such user found with ROLE_USER role");
            }
        } else {
            throw new UsernameNotFoundException("No such user found");
        }
    }

//    @Override
//    public List<UserAccount> findAll(String ) {
//        UUID convertedId = UUID.fromString(userId);
//        return userRepo.findByUserViewAccountId(convertedId);
//    }

    @Override
    public UserAccount saveUser(UserDto user) {
        UserAccount nUser = dtoUserToUser(user);
        return userRepo.save(nUser);
    }

    @Override
    public UserViewAccount saveUserView(UserViewDto userViewDto) {
        UserViewAccount userView = dtoUserViewToUserView(userViewDto);
        userView.setPassword(bcryptEncoder.encode(userViewDto.getPassword()));
        return userViewRepo.save(userView);
    }

    public UserAccount dtoUserToUser(UserDto userDTO) {
        return this.modelMapper.map(userDTO, UserAccount.class);
    }

    public UserViewAccount dtoUserViewToUserView(UserViewDto userViewDto) {
        return this.modelMapper.map(userViewDto, UserViewAccount.class);
    }
}
