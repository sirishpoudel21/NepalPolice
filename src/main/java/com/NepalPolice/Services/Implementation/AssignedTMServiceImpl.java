package com.NepalPolice.Services.Implementation;

import com.NepalPolice.Entities.*;
import com.NepalPolice.Payloads.AssignedDto;
import com.NepalPolice.Payloads.AssignedTeamMemberDto;
import com.NepalPolice.Payloads.UserDto;
import com.NepalPolice.Repositery.AssignedRepo;
import com.NepalPolice.Repositery.AssignedTMRepo;
import com.NepalPolice.Repositery.TaskRepo;
import com.NepalPolice.Repositery.TeamMemberRepo;
import com.NepalPolice.Services.AssignedTMService;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.crossstore.ChangeSetPersister;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;
import java.util.UUID;
import java.util.stream.Collectors;

@Service
public class AssignedTMServiceImpl implements AssignedTMService {

    @Autowired
    private AssignedTMRepo assignedTMRepo;

    @Autowired
    private TeamMemberRepo teamMemberRepo;

    @Autowired
    private TaskRepo taskRepo;

    @Autowired
    private AssignedRepo assignedRepo;

    @Autowired
    private ModelMapper modelMapper;

    @Override
    public AssignedTeamMember createAssignedTeamMember(AssignedTeamMemberDto assignedTeamMemberDto) {
        AssignedTeamMember assignedTeamMember = dtoAssignedTmServiceToAssignedTmService(assignedTeamMemberDto);
        Optional<Task> taskOptional = taskRepo.findById(UUID.fromString(assignedTeamMemberDto.getTask()));
        Optional<TeamMember> teamMemberOptional = teamMemberRepo.findById(UUID.fromString(assignedTeamMemberDto.getTeamMember()));
        if (taskOptional.isPresent() && teamMemberOptional.isPresent()) {
            Task task = taskOptional.get();
            TeamMember teamMember = teamMemberOptional.get();
            Assigned assigned = new Assigned();
            assigned.setTask(task);
            Assigned savedAssigned = assignedRepo.save(assigned);
            assignedTeamMember.setTeamMember(teamMember);
            assignedTeamMember.setAssigned(savedAssigned);
            return assignedTMRepo.save(assignedTeamMember);
        } else {
            return null;
        }
    }

    @Override
    public List<Project> getAssignedTeamMember(UUID userId,String role) {
        List<AssignedTeamMember> assignedTeamMembers = assignedTMRepo.findByTeamMemberUserId(userId);

        List<Project> projects = assignedTeamMembers.stream()
                .map(AssignedTeamMember::getAssigned)
                .map(Assigned::getTask)
                .map(Task::getProject)
                .distinct()
                .collect(Collectors.toList());

        return projects;
    }

    public AssignedTeamMember dtoAssignedTmServiceToAssignedTmService(AssignedTeamMemberDto assignedTeamMemberDto){
        return this.modelMapper.map(assignedTeamMemberDto , AssignedTeamMember.class);
    }
    public Assigned dtoAssignedToAssigned(Assigned assigned){
        return this.modelMapper.map(assigned , Assigned.class);
    }
}
