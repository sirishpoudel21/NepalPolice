package com.NepalPolice.Services.Implementation;


import com.NepalPolice.Entities.Project;
import com.NepalPolice.Entities.Task;
import com.NepalPolice.Payloads.TaskDTO;
import com.NepalPolice.Repositery.ProjectRepo;
import com.NepalPolice.Repositery.TaskRepo;
import com.NepalPolice.Services.TaskService;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.swing.text.html.Option;
import java.util.List;
import java.util.NoSuchElementException;
import java.util.Optional;
import java.util.UUID;
import java.util.stream.Collectors;

@Service
public class TaskServiceImpl implements TaskService {
    @Autowired
    private TaskRepo taskRepo;

    @Autowired
    private ProjectRepo projectRepo;

    @Autowired
    private ModelMapper modelMapper;

    @Override
    public Task createTask(TaskDTO taskDTO) {
        UUID projectID = UUID.fromString(taskDTO.getProjectID());
        Task newTask = dtoTaskToTask(taskDTO);
        Project project = projectRepo.findById(projectID)
                .orElseThrow(() -> new IllegalArgumentException("Project Id not found!!"));
        newTask.setProject(project);
        return taskRepo.save(newTask);
    }

    @Override
    public void updateTask(TaskDTO taskDto, UUID taskID) {
        Optional<Task> optionalTask = taskRepo.findById(taskID);
        optionalTask.ifPresent(task -> {
            task.setTaskName(taskDto.getTaskName());
            task.setTaskPriority(taskDto.getTaskPriority());
            task.setTaskDescription(taskDto.getTaskDescription());
            task.setTaskPlannedStartDate(taskDto.getTaskPlannedStartDate());
            task.setTaskPlannedEndDate(taskDto.getTaskPlannedEndDate());
            task.setTaskPlannedBudget(taskDto.getTaskPlannedBudget());
            task.setTaskActualStartDate(taskDto.getTaskActualStartDate());
            task.setTaskActualEndDate(taskDto.getTaskActualEndDate());
            task.setTaskActualBudget(taskDto.getTaskActualBudget());
            taskRepo.save(task);
        });
    }

    @Override
    public Optional<Task> getTasksById(UUID taskId) {
        Optional<Task> getTaskById = taskRepo.findById(taskId);
        return getTaskById;
    }

    @Override
    public List<Task> getAllTasks() {
        return taskRepo.findAll();
    }

    @Override
    public List<TaskDTO> taskssBelongToProject(UUID projectId) {
        return null;
    }

    @Override
    public void deleteTask(UUID taskId) {
        taskRepo.deleteById(taskId);
    }

    // convert TaskDto to task
    public Task dtoTaskToTask(TaskDTO taskDto) {
        return this.modelMapper.map(taskDto, Task.class);
    }

    // Convert Task To TaskDto
    public TaskDTO taskToTaskDTO(Task task) {
        return this.modelMapper.map(task, TaskDTO.class);
    }
}
