package com.NepalPolice.Services.Implementation;

import com.NepalPolice.Entities.MunicipalityMasterEntity;
import com.NepalPolice.Services.MunicipalityMService;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

public class MunicipalityMServiceImpl implements MunicipalityMService {
    @Override
    public MunicipalityMasterEntity createMunicipalityMasterEntity(MunicipalityMasterEntity districtMChannel) {
        return null;
    }

    @Override
    public void updateMunicipalityMasterEntity(MunicipalityMasterEntity districtMChannel, UUID muncipilityId) {

    }

    @Override
    public Optional<MunicipalityMasterEntity> getMunicipalityMasterEntityById(UUID muncipilityId) {
        return Optional.empty();
    }

    @Override
    public List<MunicipalityMasterEntity> getAllMunicipalityMasterEntity() {
        return null;
    }

    @Override
    public void deleteMunicipalityMasterEntity(UUID muncipilityId) {

    }
}
