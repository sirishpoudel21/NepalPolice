package com.NepalPolice.Services.Implementation;

import com.NepalPolice.ENUMS.RoleCode;
import com.NepalPolice.Entities.Role;
import com.NepalPolice.Entities.Task;
import com.NepalPolice.Payloads.RoleDto;
import com.NepalPolice.Payloads.TaskDTO;
import com.NepalPolice.Repositery.RoleRepo;
import com.NepalPolice.Services.RoleService;
import org.apache.catalina.core.JreMemoryLeakPreventionListener;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

@Service(value = "roleService")
public class RoleServiceImpl implements RoleService {

    @Autowired
    private RoleRepo roleRepo;

    @Autowired
    private ModelMapper modelMapper;

    @Override
    public Role createRole(RoleDto roleDto, String roleCode) {
        Role existingRole = roleRepo.findByRoleName(roleDto.getRole_name());
        if (existingRole != null) {
            throw new IllegalArgumentException("Role with the same name already exists: " + roleDto.getRole_name());
        }
        roleDto.setRoleCode(getRoleCodeFromRole(roleCode));
        Role role = dtoRoleToRole(roleDto);
        return roleRepo.save(role);
    }

    @Override
    public List<Role> findAllRole() {
        return roleRepo.findAll();
    }

    @Override
    public Role findById(String roleId) {
        UUID convertedRoleId = UUID.fromString(roleId);
        return roleRepo.findById(convertedRoleId).orElse(null);
    }

    @Override
    public Role updateRole(String roleId, RoleDto roleDto, String roles) {
        UUID convertedRoleId = UUID.fromString(roleId);
        Optional<Role> optionalRole = roleRepo.findById(convertedRoleId);
        if (optionalRole.isPresent()) {
            Role role = optionalRole.get();
            String updatedRoleName = roleDto.getRole_name();
            if (!role.getRole_name().equals(updatedRoleName)) {
                Role existingRole = roleRepo.findByRoleName(updatedRoleName);
                if (existingRole != null && !existingRole.getId().equals(role.getId())) {
                    throw new IllegalArgumentException("Role with the same name already exists: " + updatedRoleName);
                }
            }
            role.setRole_name(updatedRoleName);
            role.setRoleCode(getRoleCodeFromRole(roles));
            role.setAccessModifier(roleDto.getAccessModifier());
            return roleRepo.save(role);
        } else {
            throw new RuntimeException("Role not found with ID: " + roleId);
        }
    }

    @Override
    public void deleteRoleById(String roleId) {
        UUID convertedRoleId = UUID.fromString(roleId);
        roleRepo.deleteById(convertedRoleId);
    }

    @Override
    public Role findByRoleName(String name) {
        Role role = roleRepo.findByRoleName(name);
        return role;
    }

    private RoleCode getRoleCodeFromRole(String role) {
        switch (role) {
            case "ADMIN":
                return RoleCode.ADMIN;
            case "ROLE_SUPER_ADMIN":
                return RoleCode.SUPER_ADMIN;
            case "ROLE_PROJECT_MANAGER":
                return RoleCode.PROJECT_MANAGER;
            default:
                throw new IllegalArgumentException("Invalid role: " + role);
        }
    }

    public Role dtoRoleToRole(RoleDto roleDto) {
        return this.modelMapper.map(roleDto, Role.class);
    }
}
