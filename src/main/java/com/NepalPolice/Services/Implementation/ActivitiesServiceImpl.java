package com.NepalPolice.Services.Implementation;

import com.NepalPolice.ENUMS.Status;
import com.NepalPolice.ENUMS.VerifyStatus;
import com.NepalPolice.Entities.Activity;
import com.NepalPolice.Entities.Task;
import com.NepalPolice.Payloads.ActivitiesDTO;
import com.NepalPolice.Repositery.ActivitiesRepo;
import com.NepalPolice.Repositery.TaskRepo;
import com.NepalPolice.Services.ActivitiesService;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.StringUtils;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

@Service
public class ActivitiesServiceImpl implements ActivitiesService {

    @Autowired
    private ActivitiesRepo activitiesRepo;

    @Autowired
    private ModelMapper modelMapper;

    @Autowired
    private TaskRepo taskRepo;

    private static final String UPLOAD_DIRECTORY = "report/";

    @Override
    public Activity createActivities(ActivitiesDTO activitiesDTO, MultipartFile files) {
        try {
            String fileUrls = saveFiles(files);

            UUID taskId = UUID.fromString(activitiesDTO.getTaskId());
            Activity newActivity = dtoActivitiesToActivity(activitiesDTO);

            Task task = taskRepo.findById(taskId)
                    .orElseThrow(() -> new IllegalArgumentException("Task Id not found!!"));
            newActivity.setTask(task);

            newActivity.setActivityStatus(Status.ONGOING);
            newActivity.setVerifyStatus(VerifyStatus.NOT_VERIFIED);
            newActivity.setFilePath(fileUrls);

            return activitiesRepo.save(newActivity);
        } catch (IOException ex) {
            throw new IllegalArgumentException("Failed to save file: " + ex.getMessage());
        }
    }

    private String saveFiles(MultipartFile file) throws IOException {
        File directory = new File(UPLOAD_DIRECTORY);
        if (!directory.exists()) {
            directory.mkdirs();
        }

        String timestamp = LocalDateTime.now().format(DateTimeFormatter.ofPattern("HH_mm_ss"));

        String originalFilename = file.getOriginalFilename();
        String fileExtension = originalFilename.substring(originalFilename.lastIndexOf('.'));
        String newFilename = timestamp + "_" + originalFilename;
        String filepath = UPLOAD_DIRECTORY + File.separator + newFilename;
        File destFile = new File(filepath);

        // Save the file
        file.transferTo(destFile);

        // Return the file path
        return filepath;
    }

    @Override
    public void updateActivities(ActivitiesDTO activitiesDTO, UUID activityId) {
        Optional<Activity> optionalActivity = activitiesRepo.findById(activityId);
        optionalActivity.ifPresent(activity -> {
            activity.setActivityName(activitiesDTO.getActivityName());
            activity.setActivityDescription(activitiesDTO.getActivityDescription());
            activity.setActivityPriority(activitiesDTO.getActivityPriority());
            activitiesRepo.save(activity);
        });
    }

    @Override
    public Optional<Activity> getActivitiesById(UUID activitiesID) {
        Optional<Activity> getActivityById = activitiesRepo.findById(activitiesID);
        return getActivityById;
    }

    @Override
    public List<Activity> getAllActivities() {
        return activitiesRepo.findAll();
    }

    @Override
    public List<Activity> getAllActivitiesByTaskId(UUID taskId){
        return activitiesRepo.findActivitiesByTaskId(taskId);
    }

    @Override
    public List<Activity> getActivitiesForPm(UUID userId) {
        return null;
    }

    @Override
    public List<ActivitiesDTO> activitiesBelongToTask(UUID activitiesId) {
        return null;
    }

    @Override
    public void deleteActivities(UUID activitiesId) {
        activitiesRepo.deleteById(activitiesId);
    }

    // convert activitiesDTO to activities
    public Activity dtoActivitiesToActivity(ActivitiesDTO activitiesDTO){
        return this.modelMapper.map(activitiesDTO ,Activity.class);
    }

    // Convert activities To ActivitiesDto
    public ActivitiesDTO activityToActivitiesDTO(Activity activity){
        return this.modelMapper.map(activity, ActivitiesDTO.class);
    }
}
