package com.NepalPolice.Services.Implementation;

import com.NepalPolice.Entities.Role;
import com.NepalPolice.Entities.Team;
import com.NepalPolice.Entities.TeamMember;
import com.NepalPolice.Entities.UserAccount;
import com.NepalPolice.Payloads.RoleDto;
import com.NepalPolice.Payloads.TeamMemberDto;
import com.NepalPolice.Repositery.RoleRepo;
import com.NepalPolice.Repositery.TeamMemberRepo;
import com.NepalPolice.Repositery.TeamRepo;
import com.NepalPolice.Repositery.UserRepo;
import com.NepalPolice.Services.TeamMemberService;
import com.NepalPolice.UserEntity.UserViewAccount;
import com.NepalPolice.UserRepositery.UserViewRepo;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

@Service
public class TeamMemberServiceImpl implements TeamMemberService {
    @Autowired
    private TeamMemberRepo teamMemberRepo;

    @Autowired
    private RoleRepo roleRepo;

    @Autowired
    private UserViewRepo userViewRepo;

    @Autowired
    private TeamRepo teamRepo;

    @Autowired
    private ModelMapper modelMapper;

    @Override
    public TeamMember createTeamMember(TeamMemberDto teamMemberDto) {
        Team team = teamRepo.findById(UUID.fromString(teamMemberDto.getTeam()))
                .orElseThrow(() -> new RuntimeException("Team not found"));
        Optional<Role> role = roleRepo.findById(UUID.fromString(teamMemberDto.getRole()));
//                .orElseThrow(() -> new RuntimeException("Role not found"));
        Optional<UserViewAccount> user = userViewRepo.findById(UUID.fromString(teamMemberDto.getUserId()));
        if (user == null) {
            throw new RuntimeException("User not found");
        }
        TeamMember teamMember = dtoTeamMemberToTeamMember(teamMemberDto);
        teamMember.setRole(role.get());
        teamMember.setTeam(team);
        teamMember.setUserId(user.get().getId());
        return teamMemberRepo.save(teamMember);
    }

    @Override
    public TeamMember updateTeamMember(TeamMemberDto teamMemberDto, UUID teamMemberID) {
//        TeamMember existingTeamMember = teamMemberRepo.findById(teamMemberID)
//                .orElseThrow(() -> new RuntimeException("Team Member not found"));
//        UserAccount user = userRepo.findByUserViewAccountId(UUID.fromString(teamMemberDto.getUserId()));
//        if (user == null) {
//            throw new RuntimeException("User not found");
//        }
//         Team team = teamRepo.findById(UUID.fromString(teamMemberDto.getTeam()))
//                 .orElseThrow(() -> new RuntimeException("Team not found"));
//         Role role = roleRepo.findById(UUID.fromString(teamMemberDto.getRole()))
//                 .orElseThrow(() -> new RuntimeException("Role not found"));
//        existingTeamMember.setUserId(UUID.fromString(teamMemberDto.getUserId()));
//        existingTeamMember.setTeam(team);
//        existingTeamMember.setRole(role);
//        return teamMemberRepo.save(existingTeamMember);
        return null;
    }

    @Override
    public List<TeamMember> getAllTeamMember() {
        return teamMemberRepo.findAll();
    }

    @Override
    public TeamMember getTeamMemberById(UUID teamMemberID) {
        return teamMemberRepo.findById(teamMemberID).orElse(null);
    }

    @Override
    public void deleteTeamMember(UUID teamMemberId) {
        teamMemberRepo.deleteById(teamMemberId);
    }

    public TeamMember dtoTeamMemberToTeamMember(TeamMemberDto teamMemberDto) {
        return this.modelMapper.map(teamMemberDto, TeamMember.class);
    }
}
