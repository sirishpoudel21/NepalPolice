package com.NepalPolice.Services.Implementation;

import com.NepalPolice.Entities.*;
import com.NepalPolice.Payloads.ActivitiesDTO;
import com.NepalPolice.Payloads.TeamDto;
import com.NepalPolice.Repositery.ProjectManagerRepo;
import com.NepalPolice.Repositery.ProjectRepo;
import com.NepalPolice.Repositery.TeamRepo;
import com.NepalPolice.Repositery.UserRepo;
import com.NepalPolice.Services.TeamService;
import com.NepalPolice.UserRepositery.UserViewRepo;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Collections;
import java.util.List;
import java.util.Optional;
import java.util.UUID;
import java.util.stream.Collectors;

@Service
public class TeamServiceImpl implements TeamService {

    @Autowired
    private TeamRepo teamRepo;

    @Autowired
    private ProjectRepo projectRepo;

    @Autowired
    private UserRepo userRepo;

    @Autowired
    private ProjectManagerRepo projectManagerRepo;

    @Autowired
    private ModelMapper modelMapper;

    @Override
    public Team createTeam(String projectId, TeamDto teamDto) {
        UUID convertedProjectId = UUID.fromString(projectId);
        Optional<Project> optionalProject = projectRepo.findById(convertedProjectId);
        if (optionalProject.isPresent()) {
            Project project = optionalProject.get();
            Team team = dtoTeamToTeam(teamDto);
            team = teamRepo.save(team);
            project.setTeam(team);
            projectRepo.save(project);
            return team;
        } else {
            return null;
        }
    }

    @Override
    public void updateTeam(TeamDto teamDto, UUID teamID) {
        Optional<Team> optionalTeam = teamRepo.findById(teamID);
        optionalTeam.ifPresent(team -> {
            team.setTeamName(teamDto.getTeamName());
            teamRepo.save(team);
        });
    }

    @Override
    public List<Team> getAllTeam() {
        return teamRepo.findAll();
    }

    @Override
    public Optional<Team> getTeamById(UUID teamID) {
        return teamRepo.findById(teamID);
    }

    @Override
    public List<Team> getTeamByPMId(UUID pmId) {
        UserAccount userAccount = userRepo.findByUserViewAccountId(pmId);
        List<Project> projects = projectManagerRepo.findByUserAccount_Id(userAccount.getId());
        if(projects.isEmpty()) {
            throw new RuntimeException("No projects found for the given project manager ID");
        }
        List<Team> teams = projects.stream()
                .map(Project::getTeam)
                .collect(Collectors.toList());

        return teams;
    }

    @Override
    public List<Team> getTeamByProjectId(UUID userId, UUID projectId) {
        UserAccount userAccount = userRepo.findByUserViewAccountId(userId);
        Optional<Project> projectOptional = projectRepo.findById(projectId);
        if (userAccount != null && projectOptional.isPresent()) {
            Project project = projectOptional.get();

            List<Team> teams = project.getProjectManagers().stream()
                    .filter(manager -> manager.getUser_account().getId().equals(userAccount.getId()))
                    .map(ProjectManager::getProject)
                    .map(Project::getTeam)
                    .collect(Collectors.toList());
            return teams;
        }
        return Collections.emptyList();
    }


    @Override
    public void deleteTeam(UUID teamId) {
        teamRepo.deleteById(teamId);
    }

    public Team dtoTeamToTeam(TeamDto teamDto){
        return this.modelMapper.map(teamDto ,Team.class);
    }
}
