package com.NepalPolice.Services;

import com.NepalPolice.Entities.PrecedingTask;
import com.NepalPolice.Payloads.ActivitiesDTO;

import java.util.List;
import java.util.UUID;

public interface PrecedingTaskService {

    // Create Preceding_Task
    PrecedingTask createPrecedingTask(PrecedingTask precedingTask);

    // update a particular PrecedingTask by its id
    PrecedingTask updatePrecedingTask(PrecedingTask precedingTask, UUID precedingTaskID);

    // get a single PrecedingTask by its id
    PrecedingTask getPrecedingTaskById(UUID TaskID);

    // Get all PrecedingTask Belong to Task
    List<PrecedingTask> allPrecedingTaskBYTaskID(UUID taskId);

    // delete PrecedingTask by its I'd
    void deletePrecedingTask(UUID precedingTaskId);
}
