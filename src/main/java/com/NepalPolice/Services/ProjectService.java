package com.NepalPolice.Services;

import com.NepalPolice.Entities.Project;
import com.NepalPolice.Payloads.ProjectDTO;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

public interface ProjectService {

    // Create project
    Project createProject(Project project);

    // update a particular project by its id
    void updateProject(ProjectDTO projectDto, UUID projectId);

    // get a single project date buy its id
    Optional<Project> getProjectById(UUID projectId);

    // Get all  projects
    List<Project> getAllProjects();

    List<Project> getProjectByTeamId(UUID teamId);


    // Get projects Belong to manager
    List<ProjectDTO> projectsBelongToManager(UUID managerID);

    // delete Project by its I'd
    void deleteProject(UUID projectId);
}
