package com.NepalPolice.Services;

import com.NepalPolice.Entities.Task;
import com.NepalPolice.Payloads.TaskDTO;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

public interface TaskService {
    // Create Task
    Task createTask(TaskDTO taskDTO);

    // update a particular Task by its id
    void updateTask(TaskDTO taskDto, UUID taskID);

    // get a single Task by its id
    Optional<Task> getTasksById(UUID taskId);

    // Get all  projects
    List<Task> getAllTasks();

    // Get Tasks Belong to project
    List<TaskDTO> taskssBelongToProject(UUID projectId);

    // delete Task by its I'd
    void deleteTask(UUID taskId);
}
