package com.NepalPolice.Services;

import com.NepalPolice.Entities.Team;
import com.NepalPolice.Payloads.TeamDto;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

public interface TeamService {
    // create Team
    Team createTeam(String projectId,TeamDto teamDto);

    // update Team
    void updateTeam(TeamDto teamDto, UUID teamID);

    // Get all  Team
    List<Team> getAllTeam();

    // Get Team by id
    Optional<Team> getTeamById(UUID teamID);

    List<Team> getTeamByPMId(UUID pmId);

    List<Team> getTeamByProjectId(UUID userId, UUID projectId);

    // delete Team by its I'd
    void deleteTeam(UUID teamId);
}
