package com.NepalPolice.Services;

import com.NepalPolice.Entities.DistrictMChannel;
import com.NepalPolice.Entities.MunicipalityMasterEntity;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

public interface MunicipalityMService {
    // Create MuncipalityMasterEntity
    MunicipalityMasterEntity createMunicipalityMasterEntity(MunicipalityMasterEntity districtMChannel);

    // update a particular MunicipalityMasterEntity by its id
    void updateMunicipalityMasterEntity(MunicipalityMasterEntity districtMChannel, UUID muncipilityId);

    // get a single MuncipalityMasterEntity
    Optional<MunicipalityMasterEntity> getMunicipalityMasterEntityById(UUID muncipilityId);

    // Get all  MuncipalityMasterEntity
    List<MunicipalityMasterEntity> getAllMunicipalityMasterEntity();

    // delete MuncipalityMasterEntity by its I'd
    void deleteMunicipalityMasterEntity(UUID municipilityId);
}
