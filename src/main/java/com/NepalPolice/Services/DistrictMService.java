package com.NepalPolice.Services;

import com.NepalPolice.Entities.DistrictMChannel;
import com.NepalPolice.Entities.Project;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

public interface DistrictMService {
    // Create DistrictMChannel
    DistrictMChannel createDistrictMChannel(DistrictMChannel districtMChannel);

    // update a particular DistrictMChannel by its id
    void updateDistrictMChannelService(DistrictMChannel districtMChannel, UUID districtId);

    // get a single DistrictMChannel
    Optional<DistrictMChannel> getDistrictMChannelById(UUID districtId);

    // Get all  DistrictMChannel
    List<DistrictMChannel> getAllDistrictMChannel();

    // delete DistrictMChannel by its I'd
    void deleteDistrictMChannel(UUID districtId);
}
