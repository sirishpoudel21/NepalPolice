package com.NepalPolice.Services;

import com.NepalPolice.Entities.LoginCrediential;
import com.NepalPolice.Entities.UserAccount;
import com.NepalPolice.Payloads.UserDto;
import com.NepalPolice.Payloads.UserViewDto;
import com.NepalPolice.UserEntity.UserViewAccount;
import org.springframework.security.core.userdetails.UserDetails;

import java.util.List;
import java.util.Optional;

public interface UserService {
    UserAccount saveUser(UserDto user);

    UserViewAccount findOne(String userId);
    UserViewAccount findOneById(String userId);
    List<UserViewAccount> findByUserName();

    UserViewAccount saveUserView(UserViewDto userViewDto);
}
