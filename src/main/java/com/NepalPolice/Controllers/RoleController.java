package com.NepalPolice.Controllers;

import com.NepalPolice.ENUMS.RoleCode;
import com.NepalPolice.Entities.*;
import com.NepalPolice.Payloads.RoleDto;
import com.NepalPolice.Payloads.TaskDTO;
import com.NepalPolice.Payloads.TeamDto;
import com.NepalPolice.Services.RoleService;
import com.NepalPolice.Services.TeamService;
import com.NepalPolice.security.JwtHelper;
import io.jsonwebtoken.Claims;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.UUID;

@RestController
@CrossOrigin
@RequestMapping("/api/v1/")
public class RoleController {
    @Autowired
    private RoleService roleService;

    @Autowired
    private JwtHelper jwtHelper;

    @PreAuthorize("hasRole('PROJECT_MANAGER')")
    @PostMapping("/role")
    public ResponseEntity<?> createRole(@RequestBody RoleDto roleDto, @RequestHeader("Authorization") String token) {
        try {
            if (token != null && token.startsWith("Bearer ")) {
                String jwtToken = token.substring(7);
                Claims claims = jwtHelper.getAllClaimsFromToken(jwtToken);
                List<String> roles = claims.get("roles", List.class);
                String rolesAsString = String.join(",", roles);
                return ResponseEntity.status(HttpStatus.ACCEPTED).body(roleService.createRole(roleDto, rolesAsString));
            } else {
                return ResponseEntity.status(HttpStatus.BAD_REQUEST)
                        .body("Invalid or missing authorization token");
            }

        } catch (Exception ex) {
            return ResponseEntity.status(HttpStatus.BAD_REQUEST)
                    .body(ex.getMessage());
        }
    }

    @PreAuthorize("hasRole('PROJECT_MANAGER')")
    @GetMapping("/role")
    public ResponseEntity<?> getAllRole(){
        try {
            return ResponseEntity.status(HttpStatus.OK).body(roleService.findAllRole());
        } catch (Exception ex) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("Failed to retrieve role by ID");
        }
    }

    @PreAuthorize("hasRole('PROJECT_MANAGER')")
    @GetMapping("/role/{roleId}")
    public ResponseEntity<?> getRoleById(@PathVariable String roleId){
        try {
            return ResponseEntity.status(HttpStatus.OK).body(roleService.findById(roleId));
        } catch (Exception ex) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("Failed to retrieve role by ID");
        }
    }

    @PreAuthorize("hasRole('PROJECT_MANAGER')")
    @PutMapping("/role/{roleId}")
    @ResponseBody
    public ResponseEntity<?> updateTask(@PathVariable String roleId,@RequestBody RoleDto roleDto, @RequestHeader("Authorization") String token){
        try {
            if (token != null && token.startsWith("Bearer ")) {
                String jwtToken = token.substring(7);
                Claims claims = jwtHelper.getAllClaimsFromToken(jwtToken);
                List<String> roles = claims.get("roles", List.class);
                String rolesAsString = String.join(",", roles);
                return ResponseEntity.status(HttpStatus.ACCEPTED).body(roleService.updateRole(roleId, roleDto, rolesAsString));
            } else {
                throw new IllegalArgumentException("Invalid or missing authorization token");
            }
        }
        catch (Exception e) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(e.getMessage().toString());
        }
    }

    @PreAuthorize("hasRole('PROJECT_MANAGER')")
    @DeleteMapping("/role/{roleId}")
    @ResponseBody
    public ResponseEntity<?> deleteRole(@PathVariable String roleId){
        try{
            roleService.deleteRoleById(roleId);
            return new ResponseEntity<>("project deleted", HttpStatus.OK);
        } catch (Exception e){
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(e.getMessage());
        }
    }
}
