package com.NepalPolice.Controllers;

import com.NepalPolice.Entities.Task;
import com.NepalPolice.Payloads.TaskDTO;
import com.NepalPolice.Services.TaskService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.util.UUID;

@RestController
@CrossOrigin
@RequestMapping("/api/v1")
public class TaskController {
//    @Autowired
//    private TaskServiceImpl taskServiceImplementation;

    @Autowired
    private TaskService taskService;

    @PreAuthorize("hasRole('PROJECT_MANAGER')")
    @PostMapping("/task")
    public ResponseEntity<?> createTask(@RequestBody TaskDTO taskDTO){
        try{
            Task newTask = taskService.createTask(taskDTO);
            return ResponseEntity.status(HttpStatus.CREATED).body(newTask);
        }catch (Exception e){
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(e.getMessage());
        }
    }

    @PreAuthorize("hasRole('PROJECT_MANAGER')")
    @GetMapping("/task")
    public ResponseEntity<?> getAllTask(){
        try{
            return ResponseEntity.status(HttpStatus.ACCEPTED).body(taskService.getAllTasks());
        } catch (Exception e){
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(e.getMessage());
        }
    }

    @PreAuthorize("hasRole('PROJECT_MANAGER')")
    @GetMapping("/task/{id}")
    @ResponseBody
    public ResponseEntity<?> getTaskById(@PathVariable String id){
        try{
            UUID taskId = UUID.fromString(id);
            return ResponseEntity.status(HttpStatus.ACCEPTED).body(taskService.getTasksById(taskId));
        }catch (Exception e){
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(e.getMessage());
        }
    }

    @PreAuthorize("hasRole('PROJECT_MANAGER')")
    @DeleteMapping("/task/{id}")
    @ResponseBody
    public ResponseEntity<?> deleteTask(@PathVariable String id){
        try{
            UUID taskId = UUID.fromString(id);
            taskService.deleteTask(taskId);
            return new ResponseEntity<>("task deleted", HttpStatus.OK);
        }catch (Exception e){
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(e.getMessage());
        }
    }

    @PreAuthorize("hasRole('PROJECT_MANAGER')")
    @PutMapping("/task/{id}")
    @ResponseBody
    public ResponseEntity<?> updateTask(@PathVariable String id,@RequestBody TaskDTO taskDTO){
        try {
            UUID taskId = UUID.fromString(id);
            taskService.updateTask(taskDTO, taskId);
            Task updatedTask = taskService.getTasksById(taskId)
                    .orElseThrow(()->new Exception("No such element found"));
            return ResponseEntity.status(HttpStatus.ACCEPTED).body(updatedTask);
        }
        catch (Exception e) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(e.getMessage().toString());
        }
    }
}
