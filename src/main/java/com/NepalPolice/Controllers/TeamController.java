package com.NepalPolice.Controllers;

import com.NepalPolice.Entities.Team;
import com.NepalPolice.Payloads.TeamDto;
import com.NepalPolice.Services.TeamService;
import com.NepalPolice.security.JwtHelper;
import io.jsonwebtoken.Claims;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.UUID;

@RestController
@CrossOrigin
@RequestMapping("/api/v1/")
public class TeamController {

    @Autowired
    private TeamService teamService;

    @Autowired
    private JwtHelper jwtHelper;

    @PreAuthorize("hasRole('PROJECT_MANAGER')")
    @PostMapping("/project/{projectId}")
    public ResponseEntity<?> createTeam(@PathVariable String projectId,@RequestBody TeamDto teamDto){
        try{
            Team newTeam = teamService.createTeam(projectId,teamDto);
            return ResponseEntity.status(HttpStatus.CREATED).body(newTeam);
        } catch(Exception ex){
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(ex.getMessage());
        }
    }

//    @PreAuthorize("hasRole('PROJECT_MANAGER')")
//    @GetMapping("/team")
//    public ResponseEntity<?> getAllTeam(){
//        try{
//            return ResponseEntity.status(HttpStatus.ACCEPTED).body(teamService.getAllTeam());
//        } catch (Exception e){
//            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(e.getMessage());
//        }
//    }

    @PreAuthorize("hasRole('PROJECT_MANAGER')")
    @GetMapping("/team/{id}")
    public ResponseEntity<?> getTeamById(@PathVariable String id){
        try{
            UUID teamId = UUID.fromString(id);
            return ResponseEntity.status(HttpStatus.ACCEPTED).body(teamService.getTeamById(teamId));
        }
        catch(Exception ex){
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(ex.getMessage());
        }
    }

    @PreAuthorize("hasRole('PROJECT_MANAGER')")
    @GetMapping("/team")
    public ResponseEntity<?> getTeamByPmId(@RequestHeader("Authorization") String token){
        try{
            if (token != null && token.startsWith("Bearer ")) {
                String jwtToken = token.substring(7);
                Claims claims = jwtHelper.getAllClaimsFromToken(jwtToken);
                List<String> roles = claims.get("roles", List.class);
                String rolesString = String.join(",", roles);
                UUID userId = UUID.fromString(claims.get("userId", String.class));
                if (roles != null && !roles.isEmpty()) {
                    if (roles.contains("ROLE_SUPER_ADMIN")) {
                        return ResponseEntity.status(HttpStatus.ACCEPTED).body(teamService.getAllTeam());
                    } else if (roles.contains("ROLE_PROJECT_MANAGER")) {
                        return ResponseEntity.status(HttpStatus.ACCEPTED).body(teamService.getTeamByPMId(userId));
                    } else if (roles.contains("ROLE_USER")) {
//                        return ResponseEntity.ok().body(teamService.getAssignedTeamMember(userId,rolesString));
                        return ResponseEntity.ok().body("this is user");
                    } else {
                        return ResponseEntity.status(HttpStatus.BAD_REQUEST).body("Not a valid role");
                    }
                } else {
                    return ResponseEntity.status(HttpStatus.BAD_REQUEST).body("No roles found in token");
                }
            } else {
                return ResponseEntity.status(HttpStatus.BAD_REQUEST).body("Invalid or missing authorization token");
            }
        }
        catch(Exception ex){
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(ex.getMessage());
        }
    }

    @PreAuthorize("hasRole('PROJECT_MANAGER')")
    @GetMapping("/teamByProjectId/{projectId}")
    public ResponseEntity<?> getTeamByProjectId(@RequestHeader("Authorization") String token,@PathVariable String projectId){
        try{
            if (token != null && token.startsWith("Bearer ")) {
                String jwtToken = token.substring(7);
                Claims claims = jwtHelper.getAllClaimsFromToken(jwtToken);
                List<String> roles = claims.get("roles", List.class);
                String rolesString = String.join(",", roles);
                UUID userId = UUID.fromString(claims.get("userId", String.class));
                if (roles != null && !roles.isEmpty()) {
                    if (roles.contains("ROLE_SUPER_ADMIN")) {
                        return ResponseEntity.status(HttpStatus.ACCEPTED).body(teamService.getAllTeam());
                    } else if (roles.contains("ROLE_PROJECT_MANAGER")) {
                        return ResponseEntity.status(HttpStatus.ACCEPTED).body(teamService.getTeamByProjectId(userId,UUID.fromString(projectId)));
                    } else if (roles.contains("ROLE_USER")) {
//                        return ResponseEntity.ok().body(teamService.getAssignedTeamMember(userId,rolesString));
                        return ResponseEntity.ok().body("this is user");
                    } else {
                        return ResponseEntity.status(HttpStatus.BAD_REQUEST).body("Not a valid role");
                    }
                } else {
                    return ResponseEntity.status(HttpStatus.BAD_REQUEST).body("No roles found in token");
                }
            } else {
                return ResponseEntity.status(HttpStatus.BAD_REQUEST).body("Invalid or missing authorization token");
            }
        }
        catch(Exception ex){
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(ex.getMessage());
        }
    }



//    @PreAuthorize("hasRole('PROJECT_MANAGER')")
//    @GetMapping("/team/{projectId}")
//    public ResponseEntity<?> getTeamByProjectId(@PathVariable String projectId){
//        try{
//            UUID convertedProjectId = UUID.fromString(projectId);
//            return ResponseEntity.status(HttpStatus.ACCEPTED).body(teamService.getTeamByProjectId(convertedProjectId));
//        }
//        catch(Exception ex){
//            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(ex.getMessage());
//        }
//    }

    @PreAuthorize("hasRole('PROJECT_MANAGER')")
    @PutMapping("/team/{id}")
    @ResponseBody
    public ResponseEntity<?> updateTeam(@PathVariable String id,@RequestBody TeamDto teamDto){
        try {
            UUID teamId = UUID.fromString(id);
            teamService.updateTeam(teamDto, teamId);
            Team updatedTeam = teamService.getTeamById(teamId)
                    .orElseThrow(()->new Exception("No such element found"));
            return ResponseEntity.status(HttpStatus.ACCEPTED).body(updatedTeam);
        }
        catch (Exception e) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(e.getMessage().toString());
        }
    }

    @PreAuthorize("hasRole('PROJECT_MANAGER')")
    @DeleteMapping("/team/{id}")
    @ResponseBody
    public ResponseEntity<?> deleteTeam(@PathVariable String id){
        try{
            UUID teamId = UUID.fromString(id);
            teamService.deleteTeam(teamId);
            return new ResponseEntity<>("activity deleted", HttpStatus.OK);
        } catch (Exception e){
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(e.getMessage());
        }
    }
}
