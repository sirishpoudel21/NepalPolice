package com.NepalPolice.Controllers;

import com.NepalPolice.Entities.JwtResponse;
import com.NepalPolice.Entities.LoginCrediential;
import com.NepalPolice.Entities.UserAccount;
import com.NepalPolice.Payloads.EmployeeDto;
import com.NepalPolice.Payloads.UserDto;
import com.NepalPolice.Payloads.UserViewDto;
import com.NepalPolice.Repositery.UserRepo;
import com.NepalPolice.Services.EmployeeService;
import com.NepalPolice.Services.Implementation.UserServiceImpl;
import com.NepalPolice.Services.UserService;
import com.NepalPolice.UserEntity.UserViewAccount;
import com.NepalPolice.security.JwtHelper;
import jakarta.annotation.security.RolesAllowed;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.annotation.Secured;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

@CrossOrigin
@RestController
public class LoginController {

    @Autowired
    private UserService userService;

    @Autowired
    private EmployeeService employeeService;

    @Autowired
    private UserDetailsService userDetailsService;

    @Autowired
    private AuthenticationManager manager;

    @Autowired
    private JwtHelper helper;

    private Logger logger = LoggerFactory.getLogger(LoginController.class);

    @PostMapping("/auth/login")
    public ResponseEntity<?> login(@RequestBody LoginCrediential request) {
        this.doAuthenticate(request.getUsername(), request.getPassword());
        UserDetails userDetails = userDetailsService.loadUserByUsername(request.getUsername());
        String token = this.helper.generateToken(userDetails);
        JwtResponse response = JwtResponse.builder()
                .token(token)
                .username(userDetails.getUsername())
                .role(userDetails.getAuthorities().toString()).build();
        return new ResponseEntity<>(response, HttpStatus.OK);
    }

    private void doAuthenticate(String username, String password) {

        UsernamePasswordAuthenticationToken authentication = new UsernamePasswordAuthenticationToken(username, password);
        try {
            manager.authenticate(authentication);
        } catch (BadCredentialsException e) {
            throw new BadCredentialsException(" Invalid Username or Password  !!");
        }
    }

    @ExceptionHandler(BadCredentialsException.class)
    public String exceptionHandler() {
        return "Credentials Invalid !!";
    }

    @PostMapping(value = "/auth/register")
    public UserAccount saveUser(@RequestBody UserDto user) {
        return userService.saveUser(user);
    }

    @PostMapping(value = "/auth/registerview")
    public ResponseEntity<?> saveUserViewAccount(@RequestBody UserViewDto userViewDto) {
        try {
            return ResponseEntity.status(HttpStatus.ACCEPTED).body(userService.saveUserView(userViewDto));
        } catch (Exception ex) {
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(ex.getMessage());
        }
    }

    @PostMapping(value = "/auth/employee")
    public ResponseEntity<?> saveEmployee(@RequestBody EmployeeDto employeeDto) {
        try {
            return ResponseEntity.status(HttpStatus.ACCEPTED).body(employeeService.createEmployee(employeeDto));
        } catch (Exception ex) {
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(ex.getMessage());
        }
    }
}
