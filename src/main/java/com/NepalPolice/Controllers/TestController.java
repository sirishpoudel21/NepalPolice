package com.NepalPolice.Controllers;

import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@CrossOrigin
@RestController
public class TestController {

    @PreAuthorize("hasRole('PROJECT_MANAGER')")
    @GetMapping("/api/v1/testing")
    public String test(){
        return "testing";
    }
}
