package com.NepalPolice.Controllers;

import com.NepalPolice.Entities.Team;
import com.NepalPolice.Entities.TeamMember;
import com.NepalPolice.Payloads.TeamDto;
import com.NepalPolice.Payloads.TeamMemberDto;
import com.NepalPolice.Services.TeamMemberService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.UUID;

@RestController
@CrossOrigin
@RequestMapping("/api/v1/")
public class TeamMemberController {
    @Autowired
    private TeamMemberService teamMemberService;

    @PreAuthorize("hasRole('PROJECT_MANAGER')")
    @PostMapping("/teamMembers")
    public ResponseEntity<?> createTeamMember(@RequestBody TeamMemberDto teamMemberDto){
        try{
            TeamMember newTeamMember = teamMemberService.createTeamMember(teamMemberDto);
            return ResponseEntity.status(HttpStatus.CREATED).body(newTeamMember);
        } catch(Exception ex){
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(ex.getMessage());
        }
    }

    @PreAuthorize("hasRole('PROJECT_MANAGER')")
    @PutMapping("/teamMembers/{id}")
    public ResponseEntity<?> updateTeamMember(@RequestBody TeamMemberDto teamMemberDto, @PathVariable String id) {
        try {
            UUID convertedId = UUID.fromString(id);
            TeamMember updatedTeamMember = teamMemberService.updateTeamMember(teamMemberDto, convertedId);
            return ResponseEntity.ok(updatedTeamMember);
        } catch (Exception ex) {
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(ex.getMessage());
        }
    }

    @PreAuthorize("hasRole('PROJECT_MANAGER')")
    @GetMapping("/teamMembers")
    public ResponseEntity<List<TeamMember>> getAllTeamMembers() {
        List<TeamMember> teamMembers = teamMemberService.getAllTeamMember();
        return ResponseEntity.ok(teamMembers);
    }

    @PreAuthorize("hasRole('PROJECT_MANAGER')")
    @GetMapping("/teamMembers/{id}")
    public ResponseEntity<?> getTeamMemberById(@PathVariable String id) {
        try {
            UUID convertedId = UUID.fromString(id);
            TeamMember teamMember = teamMemberService.getTeamMemberById(convertedId);
            if (teamMember != null) {
                return ResponseEntity.ok(teamMember);
            } else {
                return ResponseEntity.notFound().build();
            }
        } catch (Exception ex) {
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(ex.getMessage());
        }
    }

    @PreAuthorize("hasRole('PROJECT_MANAGER')")
    @DeleteMapping("/teamMembers/{id}")
    public ResponseEntity<?> deleteTeamMember(@PathVariable String id) {
        try {
            UUID convertedId = UUID.fromString(id);
            teamMemberService.deleteTeamMember(convertedId);
            return ResponseEntity.ok().build();
        } catch (Exception ex) {
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(ex.getMessage());
        }
    }
}
