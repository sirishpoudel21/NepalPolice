package com.NepalPolice.Controllers;

import com.NepalPolice.Entities.OnProject;
import com.NepalPolice.Entities.Project;
import com.NepalPolice.Entities.ProjectManager;
import com.NepalPolice.Entities.UserAccount;
import com.NepalPolice.Payloads.ProjectDTO;
import com.NepalPolice.Services.*;
import com.NepalPolice.Services.Implementation.ProjectManagerServiceImpl;
import com.NepalPolice.UserEntity.UserViewAccount;
import com.NepalPolice.security.JwtHelper;
import io.jsonwebtoken.Claims;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.web.bind.annotation.*;

import java.util.*;
import java.util.stream.Collectors;

@CrossOrigin
@RestController
@RequestMapping("/api/v1/")
public class ProjectController {

    @Autowired
    private ProjectService projectService;

    @Autowired
    private UserService userService;

    @Autowired
    private TeamService teamService;

    @Autowired
    private ProjectManagerService projectManagerService;

    @Autowired
    private AssignedTMService assignedTMService;

    @Autowired
    private JwtHelper jwtHelper;

    @PreAuthorize("hasRole('PROJECT_MANAGER')")
    @PostMapping("/project")
    public ResponseEntity<Project> createProject(@RequestBody Project project, @RequestHeader("Authorization") String token){
        if (token != null && token.startsWith("Bearer ")) {
            String jwtToken = token.substring(7);
            Claims claims = jwtHelper.getAllClaimsFromToken(jwtToken);
            Project createdProject = projectService.createProject(project);
            ProjectManager insertManager = projectManagerService.insertProjectManager(claims.get("userId", String.class),project.getId());
            return ResponseEntity.status(HttpStatus.CREATED).body(createdProject);
        } else {
            throw new IllegalArgumentException("Invalid or missing authorization token");
        }
    }

    @PreAuthorize("hasRole('PROJECT_MANAGER') || hasRole('USER')")
    @GetMapping("/project")
    public ResponseEntity<?> getProject(@RequestHeader("Authorization") String token){
        if (token != null && token.startsWith("Bearer ")) {
            String jwtToken = token.substring(7);
            Claims claims = jwtHelper.getAllClaimsFromToken(jwtToken);
            // extract user roles from claims
            List<String> roles = claims.get("roles", List.class);
            String rolesString = String.join(",", roles);
            UUID userId = UUID.fromString(claims.get("userId", String.class));
            if (roles != null && !roles.isEmpty()) {
                if (roles.contains("ROLE_SUPER_ADMIN")) {
                    return ResponseEntity.ok().body(projectService.getAllProjects());
                } else if (roles.contains("ROLE_PROJECT_MANAGER")) {
                    return ResponseEntity.ok().body(projectManagerService.getProjectsForUser(userId));
                } else if (roles.contains("ROLE_USER")) {
                    return ResponseEntity.ok().body(assignedTMService.getAssignedTeamMember(userId,rolesString));
                } else {
                    return ResponseEntity.status(HttpStatus.BAD_REQUEST).body("Not a valid role");
                }
            } else {
                return ResponseEntity.status(HttpStatus.BAD_REQUEST).body("No roles found in token");
            }
        } else {
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body("Invalid or missing authorization token");
        }
    }

    @PreAuthorize("hasRole('PROJECT_MANAGER')")
    @GetMapping("/project/{id}")
    @ResponseBody
    public ResponseEntity<?> getProjectById(@PathVariable String id){
        try{
            UUID projectId = UUID.fromString(id);
            return ResponseEntity.status(HttpStatus.ACCEPTED).body(projectService.getProjectById(projectId));
        }catch (Exception e){
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(e.getMessage());
        }
    }

    @PreAuthorize("hasRole('PROJECT_MANAGER')")
    @GetMapping("/projectByTeamID/{teamId}")
    public ResponseEntity<?> getProjectByTeamId(@PathVariable String teamId){
        try{
            UUID convertedTeamId = UUID.fromString(teamId);
            return ResponseEntity.status(HttpStatus.ACCEPTED).body(projectService.getProjectByTeamId(convertedTeamId));
        }
        catch(Exception ex){
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(ex.getMessage());
        }
    }

    @PreAuthorize("hasRole('PROJECT_MANAGER')")
    @DeleteMapping("/project/{id}")
    @ResponseBody
    public ResponseEntity<?> deleteActivity(@PathVariable String id){
        try{
            UUID projectId = UUID.fromString(id);
            projectService.deleteProject(projectId);
            return new ResponseEntity<>("project deleted", HttpStatus.OK);
        } catch (Exception e){
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(e.getMessage());
        }
    }

    @PreAuthorize("hasRole('PROJECT_MANAGER')")
    @PutMapping("/project/{id}")
    @ResponseBody
    public ResponseEntity<?> updateActivity(@PathVariable String id,@RequestBody ProjectDTO projectDTO){
        try {
            UUID projectId = UUID.fromString(id);
            projectService.updateProject(projectDTO, projectId);
            Project updatedTask = projectService.getProjectById(projectId)
                    .orElseThrow(()->new Exception("No such element found"));
            return ResponseEntity.status(HttpStatus.ACCEPTED).body(updatedTask);
        }
        catch (Exception e) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(e.getMessage().toString());
        }
    }
}
