package com.NepalPolice.Controllers;

import com.NepalPolice.Entities.Activity;
import com.NepalPolice.Payloads.ActivitiesDTO;
import com.NepalPolice.Services.ActivitiesService;
import com.NepalPolice.Services.AssignedTMService;
import com.NepalPolice.security.JwtHelper;
import io.jsonwebtoken.Claims;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.util.List;
import java.util.UUID;

@RestController
@CrossOrigin
@RequestMapping("/api/v1/")
public class ActivityController {

    @Autowired
    private ActivitiesService activitiesService;

    @Autowired
    private AssignedTMService assignedTMService;

    @Autowired
    private JwtHelper jwtHelper;

//    @Value("${project.image}")
//    private String path;

    @PreAuthorize("hasRole('USER')")
    @PostMapping("/activity")
    public ResponseEntity<?> createActivity(@RequestParam("image") MultipartFile files
            , @RequestParam("activitiesDTO") ActivitiesDTO activitiesDTO){
        try{
            Activity newActivity = activitiesService.createActivities(activitiesDTO,files);
            return ResponseEntity.status(HttpStatus.CREATED).body(newActivity);
        } catch(Exception ex){
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(ex.getMessage());
        }
    }

    @PreAuthorize("hasRole('PROJECT_MANAGER') || hasRole('USER')")
    @GetMapping("/activity")
    public ResponseEntity<?> getAllActivity(@RequestHeader("Authorization") String token){
        try{
            if (token != null && token.startsWith("Bearer ")) {
                String jwtToken = token.substring(7);
                Claims claims = jwtHelper.getAllClaimsFromToken(jwtToken);
                // extract user roles from claims
                List<String> roles = claims.get("roles", List.class);
                String rolesString = String.join(",", roles);
                UUID userId = UUID.fromString(claims.get("userId", String.class));
                if (roles != null && !roles.isEmpty()) {
                    if (roles.contains("ROLE_SUPER_ADMIN")) {
                        return ResponseEntity.status(HttpStatus.ACCEPTED).body(activitiesService.getAllActivities());
                    } else if (roles.contains("ROLE_PROJECT_MANAGER")) {
                        return ResponseEntity.ok().body(activitiesService.getActivitiesForPm(userId));
                    } else if (roles.contains("ROLE_USER")) {
                        return ResponseEntity.ok().body(assignedTMService.getAssignedTeamMember(userId,rolesString));
                    } else {
                        return ResponseEntity.status(HttpStatus.BAD_REQUEST).body("Not a valid role");
                    }
                } else {
                    return ResponseEntity.status(HttpStatus.BAD_REQUEST).body("No roles found in token");
                }
            } else {
                return ResponseEntity.status(HttpStatus.BAD_REQUEST).body("Invalid or missing authorization token");
            }
        } catch (Exception e){
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(e.getMessage());
        }
    }

    @PreAuthorize("hasRole('PROJECT_MANAGER')")
    @GetMapping("/activity/{id}")
    @ResponseBody
    public ResponseEntity<?> getActivityById(@PathVariable String id){
        try{
            UUID activityId = UUID.fromString(id);
            return ResponseEntity.status(HttpStatus.ACCEPTED).body(activitiesService.getActivitiesById(activityId));
        }catch (Exception e){
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(e.getMessage());
        }
    }

    @PreAuthorize("hasRole('PROJECT_MANAGER')")
    @GetMapping("/activities/{id}")
    @ResponseBody
    public ResponseEntity<?> getAllActivitiesByTaskId(@PathVariable String id){
        try{
            UUID taskId = UUID.fromString(id);
            return ResponseEntity.status(HttpStatus.ACCEPTED).body(activitiesService.getAllActivitiesByTaskId(taskId));
        } catch (Exception e){
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(e.getMessage());
        }
    }

    @PreAuthorize("hasRole('PROJECT_MANAGER')")
    @DeleteMapping("/activity/{id}")
    @ResponseBody
    public ResponseEntity<?> deleteActivity(@PathVariable String id){
        try{
            UUID activityId = UUID.fromString(id);
            activitiesService.deleteActivities(activityId);
            return new ResponseEntity<>("activity deleted", HttpStatus.OK);
        } catch (Exception e){
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(e.getMessage());
        }
    }

    @PreAuthorize("hasRole('PROJECT_MANAGER')")
    @PutMapping("/activity/{id}")
    @ResponseBody
    public ResponseEntity<?> updateActivity(@PathVariable String id,@RequestBody ActivitiesDTO activitiesDTO){
        try {
            UUID taskId = UUID.fromString(id);
            activitiesService.updateActivities(activitiesDTO, taskId);
            Activity updatedTask = activitiesService.getActivitiesById(taskId)
                    .orElseThrow(()->new Exception("No such element found"));
            return ResponseEntity.status(HttpStatus.ACCEPTED).body(updatedTask);
        }
        catch (Exception e) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(e.getMessage().toString());
        }
    }
}
