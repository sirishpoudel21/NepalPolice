package com.NepalPolice.Controllers;

import com.NepalPolice.Entities.AssignedTeamMember;
import com.NepalPolice.Payloads.AssignedTeamMemberDto;
import com.NepalPolice.Services.AssignedTMService;
import com.NepalPolice.Services.UserService;
import com.NepalPolice.security.JwtHelper;
import io.jsonwebtoken.Claims;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.UUID;

@CrossOrigin
@RestController
@RequestMapping("/api/v1/")
public class AssignedController {
    @Autowired
    private AssignedTMService assignedTMService;

    @Autowired
    private JwtHelper jwtHelper;

    @Autowired
    private UserService userService;

    @PreAuthorize("hasRole('PROJECT_MANAGER')")
    @PostMapping("/assign")
    public ResponseEntity<?> createAssignedTeamMember(@RequestBody AssignedTeamMemberDto assignedTeamMemberDto) {
        try {
            AssignedTeamMember assignedTeamMember = assignedTMService.createAssignedTeamMember(assignedTeamMemberDto);
            if(assignedTeamMember==null){
                throw new Exception("no result found");
            }
            return ResponseEntity.ok(assignedTeamMember);
        } catch (Exception e) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR)
                    .body("Failed to create AssignedTeamMember: " + e.getMessage());
        }
    }

//    @PreAuthorize("hasRole('USER')")
//    @GetMapping("/assign")
//    public ResponseEntity<?> getAssigned(@RequestHeader("Authorization") String token) {
//        try {
//            if (token != null && token.startsWith("Bearer ")) {
//                String jwtToken = token.substring(7);
//                Claims claims = jwtHelper.getAllClaimsFromToken(jwtToken);
//                List<String> roles = claims.get("roles", List.class);
//                String rolesString = String.join(",", roles);
//                UUID userId = userService.findOne(claims.get("userId", String.class)).getId();
//                if (roles != null && !roles.isEmpty()) {
//                    return ResponseEntity.ok(assignedTMService.getAssignedTeamMember(userId, rolesString));
//                } else {
//                    return ResponseEntity.status(HttpStatus.BAD_REQUEST).body("No roles found in token");
//                }
//            } else {
//                return ResponseEntity.status(HttpStatus.BAD_REQUEST).body("Invalid or missing authorization token");
//            }
//        } catch (Exception e) {
//            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR)
//                    .body("Failed to fetch assigned data: " + e.getMessage());
//        }
//    }

}
