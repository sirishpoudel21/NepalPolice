package com.NepalPolice.Controllers;

import com.NepalPolice.Services.UserService;
import com.NepalPolice.UserEntity.UserViewAccount;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@CrossOrigin
@RestController
@RequestMapping("/api/v1/")
public class UserAndEmployeeController {

    @Autowired
    private UserService userService;


    @PreAuthorize("hasRole('PROJECT_MANAGER')")
    @GetMapping("/users")
    public ResponseEntity<?> getUser() {
        try {
            List<UserViewAccount> userViewAccounts = userService.findByUserName();
            return new ResponseEntity<>(userViewAccounts, HttpStatus.OK);
        } catch (Exception ex) {
            return new ResponseEntity<>(ex.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }


}
