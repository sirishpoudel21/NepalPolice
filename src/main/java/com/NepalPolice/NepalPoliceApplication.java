package com.NepalPolice;

import org.modelmapper.ModelMapper;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.context.annotation.Bean;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.web.servlet.config.annotation.CorsRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

@SpringBootApplication
//@EntityScan(basePackages = {"com.NepalPolice.Entities", "com.NepalPolice.UserEntity"})
public class NepalPoliceApplication {

	public static void main(String[] args) {
		SpringApplication.run(NepalPoliceApplication.class, args);
	}

	@Bean
	public ModelMapper modalMapper(){
		return new ModelMapper();
	}

}
