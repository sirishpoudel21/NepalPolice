package com.NepalPolice.Repositery;

import com.NepalPolice.Entities.TeamMember;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.UUID;

@Repository
public interface TeamMemberRepo extends JpaRepository<TeamMember, UUID> {
    List<TeamMember> findByUserId(UUID userId);
}
