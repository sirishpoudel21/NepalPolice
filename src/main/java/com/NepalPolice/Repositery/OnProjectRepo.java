package com.NepalPolice.Repositery;

import com.NepalPolice.Entities.OnProject;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.UUID;

@Repository
public interface OnProjectRepo extends JpaRepository<OnProject, UUID> {
}
