package com.NepalPolice.Repositery;

import com.NepalPolice.Entities.Project;
import com.NepalPolice.Entities.ProjectManager;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

@Repository
public interface ProjectManagerRepo extends JpaRepository<ProjectManager, UUID> {
    @Query("SELECT pm.project FROM ProjectManager pm WHERE pm.user_account.id = :userAccountId")
    List<Project> findByUserAccount_Id(UUID userAccountId);
}
