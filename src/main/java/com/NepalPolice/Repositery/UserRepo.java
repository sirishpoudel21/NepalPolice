package com.NepalPolice.Repositery;

import com.NepalPolice.Entities.LoginCrediential;
import com.NepalPolice.Entities.UserAccount;
import com.NepalPolice.UserEntity.UserViewAccount;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Repository;

import java.util.UUID;

@Repository
public interface UserRepo extends JpaRepository<UserAccount, UUID> {
    @Query("select u from UserAccount u where u.userViewAccountId = :userViewId")
    UserAccount findByUserViewAccountId(UUID userViewId);
}