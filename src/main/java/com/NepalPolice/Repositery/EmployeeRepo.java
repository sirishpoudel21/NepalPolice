package com.NepalPolice.Repositery;

import com.NepalPolice.Entities.Employee;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.UUID;

@Repository
public interface EmployeeRepo extends JpaRepository<Employee, UUID> {
    @Query("select e from Employee e where e.employee_name = :employeeName")
    Employee findByEmployee_name(String employeeName);
}
