package com.NepalPolice.Repositery;

import com.NepalPolice.Entities.Role;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.UUID;

@Repository
public interface RoleRepo extends JpaRepository<Role, UUID> {
    @Query("SELECT r FROM Role r WHERE r.role_name = :name")
    Role findByRoleName(@Param("name") String name);
}
