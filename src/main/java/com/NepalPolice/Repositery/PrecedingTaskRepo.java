package com.NepalPolice.Repositery;

import com.NepalPolice.Entities.PrecedingTask;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.UUID;

public interface PrecedingTaskRepo extends JpaRepository<PrecedingTask, UUID> {
}
