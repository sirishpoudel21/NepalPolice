package com.NepalPolice.Repositery;

import com.NepalPolice.Entities.MunicipalityMasterEntity;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.UUID;

public interface MunicipalityMRepo extends JpaRepository<MunicipalityMasterEntity, UUID> {
}
