package com.NepalPolice.Repositery;

import com.NepalPolice.Entities.AssignedTeamMember;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.UUID;

@Repository
public interface AssignedTMRepo extends JpaRepository<AssignedTeamMember, UUID> {
    List<AssignedTeamMember> findByTeamMemberUserId(UUID userId);
}
