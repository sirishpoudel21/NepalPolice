package com.NepalPolice.Repositery;

import com.NepalPolice.Entities.PrecedingActivity;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.UUID;

public interface PrecedingActivityRepo extends JpaRepository<PrecedingActivity, UUID> {
}
