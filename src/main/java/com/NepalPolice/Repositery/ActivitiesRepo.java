package com.NepalPolice.Repositery;


import com.NepalPolice.Entities.Activity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.UUID;

@Repository
public interface ActivitiesRepo extends JpaRepository<Activity, UUID> {
    @Query("select u from Activity u where u.task.id = :taskId")
    List<Activity> findActivitiesByTaskId(UUID taskId);

//    @Modifying
//    @Query("delete from Activity a where a.id = :activityId")
//    void deleteActivityById(UUID activityId);
}
