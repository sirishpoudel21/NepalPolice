package com.NepalPolice.Repositery;

import com.NepalPolice.Entities.Team;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.UUID;

@Repository
public interface TeamRepo extends JpaRepository<Team, UUID> {
    Team findByTeamName(String teamName);

//    @Query("select t from Team where t.project.id = :projectId")
//    Team findByProjectId(UUID projectId);
}
