package com.NepalPolice.Repositery;

import com.NepalPolice.Entities.Project;
import com.NepalPolice.Entities.Team;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.UUID;

@Repository
public interface ProjectRepo extends JpaRepository<Project, UUID> {
    public List<Project> findProjectByTeamId(UUID teamId);
}
