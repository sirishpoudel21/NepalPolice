package com.NepalPolice.Repositery;

import com.NepalPolice.Entities.LocationMasterChannel;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.UUID;

public interface LoactionMasterRepo extends JpaRepository<LocationMasterChannel,UUID> {
}
