package com.NepalPolice.Repositery;

import com.NepalPolice.Entities.Assigned;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.UUID;

public interface AssignedRepo extends JpaRepository<Assigned, UUID> {
}
