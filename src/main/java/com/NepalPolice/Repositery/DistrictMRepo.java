package com.NepalPolice.Repositery;

import com.NepalPolice.Entities.DistrictMChannel;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.UUID;

public interface DistrictMRepo extends JpaRepository<DistrictMChannel, UUID> {
}
