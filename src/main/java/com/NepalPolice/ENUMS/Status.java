package com.NepalPolice.ENUMS;

public enum Status {
    COMPLETED,
    ONGOING,
    PARTIALLY_COMPLETED,
    CANCELLED;
}
