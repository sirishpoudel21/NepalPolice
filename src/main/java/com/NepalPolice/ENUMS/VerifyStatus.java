package com.NepalPolice.ENUMS;

public enum VerifyStatus {
    VERIFIED,
    NOT_VERIFIED
}
