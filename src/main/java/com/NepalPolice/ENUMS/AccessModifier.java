package com.NepalPolice.ENUMS;

public enum AccessModifier {
    ALL,
    READ,
    WRITE
}
