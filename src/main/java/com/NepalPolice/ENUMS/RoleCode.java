package com.NepalPolice.ENUMS;

public enum RoleCode {
    ADMIN,
    SUPER_ADMIN,

    PROJECT_MANAGER
}
