package com.NepalPolice.ENUMS;

public enum UserStatus {
    ACTIVE,
    DEACTIVATED;
}
